from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('',
    url(r'^', include(admin.site.urls)),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^phones/', include('speedsctrl.urls')),
    #url(r'^history/', include('speedsctrl.urls'))
    url(r'^thresholds/', include('slacheck.urls')),
)
