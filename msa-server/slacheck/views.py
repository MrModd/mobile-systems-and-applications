#from django.shortcuts import render
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from django.db import transaction
from speedsctrl.models import Phone
from slacheck.models import Threshold, Entry
from slacheck.serializers import ThresholdSerializer, EntrySerializer

from datetime import *
from django.utils import timezone

# Create your views here.
@api_view(['GET'])
def thresholds(request):
	if request.method == 'GET':
		entries = Threshold.objects.all()
		serializer = ThresholdSerializer(entries, many=True)
		return Response(serializer.data)

def is_in(name, listing):
	for i in listing:
		if i.name==name:
			return True
	return False

def get_value(name, listing):
	for i in listing:
		if i.name==name:
			return i.value_float
	return -1

@api_view(['GET', 'PUT'])
def update_entry(request, pk):
	phone = None
	try:
		phone = Phone.objects.get(pk=pk)
	except Phone.DoesNotExist:
		return Response(status=status.HTTP_404_NOT_FOUND)
		
	entries = Entry.objects.all().filter(imei=phone)
	if request.method == 'GET':
		serializer = EntrySerializer(entries, many=True)
		return Response(serializer.data)
	elif request.method == 'PUT':
		thresh = Threshold.objects.all()
		serializer = None
		
		if not entries:
			serializer = EntrySerializer(data=request.data)
		else:
			serializer = EntrySerializer(entries[0], data=request.data)
		
		if serializer.is_valid():
			print serializer.validated_data
			ctrl = False
			error = str(timezone.now())+": Thresholds not respected: "
			if is_in("drain",thresh) and serializer.validated_data.get("drain") > get_value("drain", thresh):
				ctrl = True
				error += "battery drain,"
			if is_in("traffic",thresh) and serializer.validated_data.get("traffic") > get_value("traffic", thresh):
				ctrl = True
				error += " data traffic,"
			if is_in("perc_imprecision",thresh) and serializer.validated_data.get('perc_imprecision') > get_value("perc_imprecision", thresh):
				ctrl = True
				error += " percentage of inaccurate measurement,"
			if is_in("max_imprecision_interv",thresh) and serializer.validated_data.get('max_imprecision_interv') > get_value("max_imprecision_interv", thresh):
				print str(serializer.data.get('max_imprecision_interv') > get_value("max_imprecision_interv", thresh))
				ctrl = True
				error += " maximum interval of imprecision"
			
			
			serializer.save(alert=ctrl, problems=error)
			transaction.commit()
			obj = Entry.objects.get(imei=pk)
			newSer = EntrySerializer(obj)
			return Response(newSer.data)
		else:
			return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
		
