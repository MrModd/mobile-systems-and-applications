from django.db import models
from speedsctrl.models import Phone

# Create your models here.
class Threshold(models.Model):
	DRAIN = "drain"
	TRAFFIC = "traffic"
	PERC_IMP = "perc_imprecision"
	MAX_IMP_INTRV = "max_imprecision_interv"
	NAME_CHOICES = (
        (DRAIN, 'Battery drain'),
        (TRAFFIC, 'Data sent'),
        (PERC_IMP, 'Percent of imprecision'),
        (MAX_IMP_INTRV, 'Max interval of imprecision'),
    )
	name = models.CharField(primary_key=True, choices=NAME_CHOICES, max_length=50)
	value_float = models.FloatField()
	#valueInt = models.BigIntegerField()
	#drain=models.FloatField()
	#traffic=models.FloatField()
	#perc_imprecision=models.FloatField()
	#max_imprecision_interv=models.FloatField()
	
class Entry(models.Model):
	imei=models.ForeignKey('speedsctrl.Phone', unique=True)
	drain=models.FloatField()
	traffic=models.BigIntegerField()
	perc_imprecision=models.FloatField()
	max_imprecision_interv=models.BigIntegerField()
	
	alert = models.BooleanField(default=False, editable=False)
	problems = models.CharField(max_length=200, editable=False)
