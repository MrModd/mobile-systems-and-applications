from django.forms import widgets
from rest_framework import serializers
from slacheck.models import Threshold, Entry

class ThresholdSerializer(serializers.ModelSerializer):
	class Meta:
		model = Threshold
		fields = ('name', 'value_float')

class EntrySerializer(serializers.ModelSerializer):
	class Meta:
		model = Entry
		fields = ('imei', 'drain', 'traffic', 'perc_imprecision', 'max_imprecision_interv', 'alert', 'problems')
	
