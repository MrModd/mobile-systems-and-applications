from django.contrib import admin
from slacheck.models import Threshold, Entry

# Register your models here.
class ThresholdAdmin(admin.ModelAdmin):
	list_display = ('name', 'value_float')
	
class EntryAdmin(admin.ModelAdmin):
	list_display = ('imei', 'drain', 'traffic', 'perc_imprecision', 'max_imprecision_interv', 'alert', 'problems')
	
admin.site.register(Threshold, ThresholdAdmin)
admin.site.register(Entry, EntryAdmin)
