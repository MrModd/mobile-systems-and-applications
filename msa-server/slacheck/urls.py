from django.conf.urls import url, include
from slacheck import views
#import speedsctrl.views

urlpatterns = [
    url(r'^$', views.thresholds),
	url(r'^(?P<pk>[0-9]+)/$', views.update_entry),
]
