# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('speedsctrl', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Entry',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('drain', models.FloatField()),
                ('traffic', models.BigIntegerField()),
                ('bandwidth', models.FloatField()),
                ('perc_imprecision', models.FloatField()),
                ('max_imprecision_interv', models.BigIntegerField()),
                ('alert', models.BooleanField(default=False, editable=False)),
                ('problems', models.CharField(max_length=200, editable=False)),
                ('imei', models.ForeignKey(to='speedsctrl.Phone', unique=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Threshold',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=50)),
                ('value_float', models.FloatField()),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
