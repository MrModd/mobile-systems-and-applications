# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('slacheck', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='threshold',
            name='name',
            field=models.CharField(unique=True, max_length=50, choices=[(b'Bandwidth', b'bandwidth'), (b'Battery drain', b'drain'), (b'Data sent', b'traffic'), (b'Percent of imprecision', b'perc_imprecision'), (b'Max interval of imprecision', b'max_imprecision_interv')]),
            preserve_default=True,
        ),
    ]
