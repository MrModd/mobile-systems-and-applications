# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('slacheck', '0003_auto_20150429_1025'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='threshold',
            name='id',
        ),
        migrations.AlterField(
            model_name='threshold',
            name='name',
            field=models.CharField(max_length=50, serialize=False, primary_key=True, choices=[(b'bandwidth', b'Bandwidth'), (b'drain', b'Battery drain'), (b'traffic', b'Data sent'), (b'perc_imprecision', b'Percent of imprecision'), (b'max_imprecision_interv', b'Max interval of imprecision')]),
            preserve_default=True,
        ),
    ]
