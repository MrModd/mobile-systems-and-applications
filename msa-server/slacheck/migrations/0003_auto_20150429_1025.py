# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('slacheck', '0002_auto_20150429_1023'),
    ]

    operations = [
        migrations.AlterField(
            model_name='threshold',
            name='name',
            field=models.CharField(unique=True, max_length=50, choices=[(b'bandwidth', b'Bandwidth'), (b'drain', b'Battery drain'), (b'traffic', b'Data sent'), (b'perc_imprecision', b'Percent of imprecision'), (b'max_imprecision_interv', b'Max interval of imprecision')]),
            preserve_default=True,
        ),
    ]
