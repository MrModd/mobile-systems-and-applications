from django.conf.urls import url, include
from speedsctrl import views
from django.contrib import admin
#import speedsctrl.views

urlpatterns = [
    url(r'^$', views.phone_list),
	url(r'^multiphone/$', views.add_phone_bulk),
	url(r'^history/$', views.all_history),
	url(r'^history/(?P<pk>[0-9]+)/$', views.phone_history),
	url(r'^(?P<pk>[0-9]+)/$', views.add_phone_bulk),
]
