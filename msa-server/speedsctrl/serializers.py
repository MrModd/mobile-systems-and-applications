from django.forms import widgets
from rest_framework import serializers
from speedsctrl.models import Phone, History, MultiPhone

class PhoneSerializer(serializers.ModelSerializer):
	class Meta:
		model = Phone
		fields = ('imei', 'punctual_speed', 'average_speed', 'last_latitude', 'last_longitude', 'last_update', 'samples')

class HistorySerializer(serializers.ModelSerializer):
	class Meta:
		model = History
		fields = ('imei', 'hid', 'latitude', 'longitude', 'last_update', 'speed')
	
class MultiPhoneSerializer(serializers.ModelSerializer):
	phones = PhoneSerializer(many=True)
	class Meta:
		model = MultiPhone
		fields = ('phones')
