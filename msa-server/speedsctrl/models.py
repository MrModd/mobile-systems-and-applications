from django.db import models

MAX_HIST = 10

# Create your models here.
class Phone(models.Model):
	
	imei=models.CharField(max_length=15,primary_key=True)
	punctual_speed=models.FloatField(null=True, blank=True)
	average_speed=models.FloatField(null=True, blank=True)
	last_latitude=models.FloatField(null=False, blank=False)
	last_longitude=models.FloatField(null=False, blank=False)
	last_update=models.BigIntegerField(null=False, blank=False)
	samples=models.IntegerField(null=True, blank=True)
	
	def __unicode__(self):
		return "Phone " + str(self.imei)
	
	def save(self, *args, **kwargs):
		if self.punctual_speed == None:
			self.punctual_speed = 0.0
			self.average_speed = -1.0
		history = History(imei=self, latitude=self.last_latitude, longitude=self.last_longitude, \
					speed=self.punctual_speed, aver_speed=self.average_speed, last_update=self.last_update)
		history.save()
		super(Phone, self).save(*args, **kwargs)

class History(models.Model):
	
	imei=models.ForeignKey('Phone')
	hid=models.PositiveIntegerField(editable=False)
	latitude=models.FloatField()
	longitude=models.FloatField()
	last_update=models.BigIntegerField(null=False, blank=False)
	speed=models.FloatField()
	aver_speed = models.FloatField()
	
	def save(self, *args, **kwargs):
		history = History.objects.filter(imei=self.imei).order_by('-hid').values_list('hid', flat=True)
		if not history:
			new_hid = 1
		else:
			new_hid = history[0]+1
		self.hid = new_hid
		if new_hid > MAX_HIST:
			History.objects.get(imei=self.imei, hid=new_hid-MAX_HIST).delete()
		super(History, self).save(*args, **kwargs)
	
class MultiPhone(models.Model):
	phones = models.ManyToManyField('Phone', related_name='phone')
