from django.contrib import admin
from speedsctrl.models import Phone, History

# Register your models here.

class PhoneAdmin(admin.ModelAdmin):
	list_display = ('imei', 'punctual_speed', 'average_speed', 'last_update')

class HistoryAdmin(admin.ModelAdmin):
	list_display = ('imei', 'hid', 'speed')

admin.site.register(Phone, PhoneAdmin)
admin.site.register(History, HistoryAdmin)

