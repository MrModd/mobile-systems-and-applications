# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='History',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('hid', models.PositiveIntegerField(editable=False)),
                ('latitude', models.FloatField()),
                ('longitude', models.FloatField()),
                ('last_update', models.BigIntegerField()),
                ('speed', models.FloatField()),
                ('aver_speed', models.FloatField()),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='MultiPhone',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Phone',
            fields=[
                ('imei', models.CharField(max_length=15, serialize=False, primary_key=True)),
                ('punctual_speed', models.FloatField(null=True, blank=True)),
                ('average_speed', models.FloatField(null=True, blank=True)),
                ('last_latitude', models.FloatField()),
                ('last_longitude', models.FloatField()),
                ('last_update', models.BigIntegerField()),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='multiphone',
            name='phones',
            field=models.ManyToManyField(related_name='phone', to='speedsctrl.Phone'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='history',
            name='imei',
            field=models.ForeignKey(to='speedsctrl.Phone'),
            preserve_default=True,
        ),
    ]
