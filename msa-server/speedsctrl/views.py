#from django.shortcuts import render
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from speedsctrl.models import Phone, History, MultiPhone
from speedsctrl.serializers import PhoneSerializer, HistorySerializer, MultiPhoneSerializer
from slacheck.models import Entry

from datetime import *
from django.utils import timezone

import logging
from geopy.distance import vincenty


logger = logging.getLogger(__name__)

ALPHA_EMA = 0.7

@api_view(['GET', 'POST'])
def phone_list(request):
	if request.method == 'GET':
		phones = Phone.objects.all()
		serializer = PhoneSerializer(phones, many=True)
		return Response(serializer.data)
	elif request.method == 'POST':
		serializer = PhoneSerializer(data=request.data)
		if serializer.is_valid():
			serializer.save()
			return Response(serializer.data, status=status.HTTP_201_CREATED)
		return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET'])
def phone_history(request, pk):
	if request.method == 'GET':
		entries = History.objects.filter(imei=pk)
		if not entries:
			return Response(status=status.HTTP_404_NOT_FOUND)
	
		serializer = HistorySerializer(entries, many=True)
		return Response(serializer.data)

@api_view(['GET'])
def all_history(request):
	if request.method == 'GET':
		histories = History.objects.all()
		serializer = HistorySerializer(histories, many=True)
		return Response(serializer.data)
				
				
def computeSpeed(serializer):
	latitude = serializer.initial_data.get('last_latitude')
	longitude = serializer.initial_data.get('last_longitude')
	time = serializer.initial_data.get('last_update')
	samples = serializer.initial_data.get('samples')
	print samples
	
	history = History.objects.filter(imei=serializer.initial_data.get('imei')).order_by('-hid')
	o_lat = history[0].latitude
	o_long = history[0].longitude
	o_speed = history[0].speed
	o_aspeed = history[0].aver_speed
	o_time = history[0].last_update
	
	o_location = (o_lat, o_long)
	n_location = (latitude, longitude)
	
	distance = vincenty(o_location, n_location).meters
	deltaTime_in_hour = (time-o_time)/(60*60.0*1000)
	
	if distance != 0 and deltaTime_in_hour != 0:
		new_speed = (distance/1000.0)/deltaTime_in_hour
	elif distance == 0:
		new_speed = 0
	elif deltaTime_in_hour == 0:
		new_speed = o_speed
	
	print o_aspeed
	if o_aspeed != -1.0:
		ema = ALPHA_EMA*new_speed + (1-ALPHA_EMA)*o_aspeed
	else:
		ema = new_speed
	
	print ema
	n_phone = Phone(imei=serializer.initial_data.get('imei'), punctual_speed=new_speed, average_speed=ema, \
					last_latitude=latitude, last_longitude=longitude, last_update=time, samples=samples)
	
	return n_phone
				
@api_view(['GET', 'PUT'])
def add_phone_bulk(request, pk):
	try:
		phone = Phone.objects.get(pk=pk)
	except Phone.DoesNotExist:
		return Response(status=status.HTTP_404_NOT_FOUND)
	
	if request.method == 'GET':
		serializer = PhoneSerializer(phone)
		return Response(serializer.data)
	elif request.method == 'PUT':
		serializer = MultiPhoneSerializer(data=request.data)
		#if not isinstance(serializer.initial_data, str) and serializer.is_valid():
		array = serializer.initial_data.get("phones")
		for i in array:
			serialPhone = PhoneSerializer(phone, data=i)
			
			if serialPhone.is_valid():
				
				delete = False
				if serialPhone.validated_data.get("samples") < phone.samples:
					delete = True
					to_remove = History.objects.filter(imei=pk)
					for j in to_remove:
						j.delete() 
					to_remove = Entry.objects.filter(imei=pk)
					for j in to_remove:
						j.delete()
				
				
				objectPh = None
				if serialPhone.initial_data.get('punctual_speed') == None and not delete:
					objectPh = computeSpeed(serialPhone)
					serialPhone = PhoneSerializer(objectPh)
				
				print i
				if i is not array[-1]:					
					history = History(imei=Phone.objects.get(pk=pk), latitude=serialPhone.data.get('last_latitude'),\
									longitude = serialPhone.data.get('last_longitude'),\
									speed = serialPhone.data.get('punctual_speed'),\
									aver_speed = serialPhone.data.get('average_speed'),\
									last_update = serialPhone.data.get('last_update'))
					history.save()
				else:
					if objectPh == None:
						serialPhone.save()
					else:
						objectPh.save()		
			else:
				return Response(serialPhone.errors, status=status.HTTP_400_BAD_REQUEST)
		phone = Phone.objects.get(pk=pk)
		serializer = PhoneSerializer(phone)
		return Response(serializer.data)
		#else:
		#	return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
