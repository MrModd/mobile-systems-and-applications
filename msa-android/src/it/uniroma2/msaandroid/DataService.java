package it.uniroma2.msaandroid;

import it.uniroma2.msaandroid.IPC.Receiver;
import it.uniroma2.msaandroid.sqlite.DAOSendQueue;
import it.uniroma2.msaandroid.sqlite.DBParameters;
import it.uniroma2.msaandroid.sqlite.Parameter;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.sqlite.SQLiteException;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.NetworkInfo.State;
import android.os.BatteryManager;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.SystemClock;
import android.util.Log;
import android.widget.Toast;

public class DataService extends Service {
	/* ========================== TUNABLE PARAMETERS ============================== */
	
	public final static double ALPHA = 0.7; // Parameter for the Exponential Moving Average
	                                         // used in the mean speed and mean drain rate.
	public final static long POSITION_TIMEOUT = 10000; // Maximum wait time for a better position.
	                                                  // ms
	public final static long UPDATES_GOOD_STATUS = 20000; // Interval between two updates when good battery
                                                          // ms
	public final static long UPDATES_CRITICAL_STATUS = 120000; // Interval between two updates when battery is critical
	                                                           // ms
	public final static float CRITICAL = 0.4f; // Battery percentage below which it is considered critical
	public final static int GPS_POSITION_ACCURACY_THRESHOLD = 20; // Over this value a location fetched from GPS is considered coarse
	                                                               // m
	public final static int NET_POSITION_ACCURACY_THRESHOLD = 1000; // Over this value a location fetched from Network is considered coarse
                                                                     // m
	public final static long NET_TRAFFIC_INTERVAL = 30*60*1000; // Data traffic will be aggregated within this window
	                                                            // ms
	
	/* ============================================================================ */
	
	public final static String RECEIVER_TAG = "receiver"; // Used as key for IPC
	public final static String PARAMETER_TAG = "parameter_obj";
	
	private Looper mServiceLooper; // Manages a message loop for a thread
	private ServiceHandler mServiceHandler; // Executes messages sent by the looper
	
	private DBParameters db;
	private Parameter e;

	private LocationManager locationManager;
	private LocationHandler locationHandler; // Manages location updates on a separated thread
	private Location listenerLocation; // Protected by locationLock
	private static Lock locationLock = new ReentrantLock();
	
	private IPC mReceiver;
	
	private final static Lock lock = new ReentrantLock(); // This MUST be reentrant
	/* Meaning of this lock:
	 * access to alarm must be atomic.
	 * Scenario: UI thread starts service setting the alarm. Alarm is triggered and handleMessage() starts its
	 *           operations. In the meanwhile UI thread stop the service removing the alarm. handleMessage() has just
	 *           checked isRunning() method used to decide whether or not update the alarm for next message to handle.
	 *           Even if UI thread decided to stop alarm, handleMessage() see that the alarm is still running and update
	 *           it again. There's race condition on isRunning() call. 
	 */
	
	
	
	/* ===== Public methods for external calls ===== */
	
	public boolean isRunning() {
		try {
			lock.lock();
			
			Intent i = new Intent(getApplicationContext(), DataService.class);
			PendingIntent p = PendingIntent.getService(getApplicationContext(), 0, i, PendingIntent.FLAG_NO_CREATE);
			return p != null;
			
		} finally {
			lock.unlock();
		}
	}
	
	
	
	/* ===== Location listener thread handler ===== */
	
	/** This class manage the locationManager listener for location changes on a separate thread with a looper.
	 * This mechanism is necessary because using sleep method in waitForBetterPrecision() causes the thread to
	 * stall and all location updates are enqueued to the looper until waitForBetterPrecision() ends. But callbacks
	 * to onLocationChanged() are handled too late.
	 */
	private final class LocationHandler extends HandlerThread {
		private LocationListener locationListener;
		private String provider;

		public LocationHandler(String name) {
			super(name);
		}
		
		@Override
		protected void onLooperPrepared() {
			// This method is called just before the looper is started on the dedicated thread
			Log.d("DataService", "onLooperPrepared()");
			
			// Start the listener for new locations
			locationListener = new LocationListener() {
				public void onLocationChanged(Location location) {
					try {
						locationLock.lock();
						listenerLocation = location;
					} finally {
						locationLock.unlock();
					}
					Log.i("DataService", "onLocationChanged() new location: lat: " + location.getLatitude() + ", lon: " + location.getLongitude() + "; accuracy: " + location.getAccuracy());
				}
				public void onStatusChanged(String provider, int status, Bundle extras) {}
				public void onProviderEnabled(String provider) {}
				public void onProviderDisabled(String provider) {}
			};
			locationManager.requestLocationUpdates(provider, 100, 0, locationListener);
		}
		
		/**
		 * This method should be called to start the thread and initialize the listener
		 * @param provider The location provider
		 */
		public void startListening(String provider) {
			this.provider = provider;
			this.start();
		}
		
		/**
		 * Call this method to end the location manager to give updates and to end the thread
		 * @return true if the looper has been asked to quit (inherited from HandlerThread quit() method)
		 */
		public boolean stopListening() {
			locationManager.removeUpdates(locationListener);
			try {
				locationLock.lock();
				listenerLocation = null;
			} finally {
				locationLock.unlock();
			}
			locationListener = null;
			
			return this.quit();
		}
	}
	
	
	
	/* ===== Service operations ===== */
	
	// Handler that receive the messages from the thread
	private final class ServiceHandler extends Handler {
		public ServiceHandler(Looper looper) {
			super(looper);
		}
		@Override
		public void handleMessage(Message msg) {
			// This method is called for every invocation of startService() on this service
			// If 2 sequential invocation was made, second one is enqueued and executed
			// at the end of first message processing
			
			Log.i("DataService", "Handling message");
			
			
			
			/* ---------------------------------- */
			
			e = db.fetchAll();
			long newTimestamp = System.currentTimeMillis();
			
			// Retrieving basic informations: battery status and network type
			
			// === Battery status ===
			IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
			Intent batteryStatus = getApplicationContext().registerReceiver(null, ifilter);
			int status = batteryStatus.getIntExtra(BatteryManager.EXTRA_STATUS, -1);
			boolean isCharging = status == BatteryManager.BATTERY_STATUS_CHARGING ||
			                     status == BatteryManager.BATTERY_STATUS_FULL;
			int level = batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
			int scale = batteryStatus.getIntExtra(BatteryManager.EXTRA_SCALE, -1);
			double batteryPct = level / (double)scale; // Value in [0,1]
			double drain; // Drain rate
			
			db.updateBattLevel(batteryPct);
			
			if (isCharging) {
				db.updateBattStatus(DBParameters.STATUS_CHARGING);
				db.updateBattDrainRate(-1.);
			}
			else {
				db.updateBattStatus(DBParameters.STATUS_DISCHARGING);
				if (e.battery_drain_rate == -1.) {
					db.updateBattDrainRate(0.);
					db.updateBattLevelTimestamp(newTimestamp);
					db.updateOldBattLevel(batteryPct);
					e = db.getCurrent();
				}
				if (e.old_battery_level - batteryPct >= 0.01) {
					Log.i("DataService", "Old batt: " + e.old_battery_level + "; New batt: " + batteryPct);
					// Compute rate
					drain = (e.old_battery_level - batteryPct) / ((newTimestamp - e.battery_level_timestamp) / 1000); // Perc. / second
					drain *= 3600; // Perc / hour
					double meanDrain = ALPHA * drain + (1 - ALPHA) * e.battery_drain_rate;
					db.updateBattDrainRate(meanDrain);
					db.updateOldBattLevel(batteryPct);
					db.updateBattLevelTimestamp(newTimestamp);
				}
			}
			e = db.getCurrent();
			
			// Useful values: isCharging, meanDrain, batteryPct
			
			// === Network type ===
			boolean isWifi = isInWifi();
			
			/* if (good battery OR battery charging)
			 *   if (in wifi)
			 *     get position by network
			 *     compute speed
			 *     send data at each update
			 *   else if (in 3G)
			 *     get position by GPS
			 *     compute speed
			 *     send aggregate data
			 *   check drain rate
			 *   if (high drain rate)
			 *     reduce update frequency
			 * 
			 * else if (battery critical)
			 *   if (in wifi)
			 *     do nothing
			 *   if (in 3G)
			 *     get position by cell
			 *     don't compute speed
			 *     send data at each update
			 *     reduce drastically update frequency
			 * 
			 */
			
			// === Executing code based on adaptivity policy ===
			if (batteryPct > CRITICAL || isCharging) { // Good battery
				Log.i("DataService", "Good battery status");
				if (isWifi) {
					Log.i("DataService", "Network: wifi");
					
					// Location by Network
					
					Location l;
					
					String p = getBestProvider(LocationManager.NETWORK_PROVIDER);
					if (p != null) {
						
						db.incrementNLocations();
						// Statistics shown on UI
						db.updateNetType(DBParameters.NET_TYPE_WIFI);
						db.updateSendType(ProxyService.MODE_SINGLE);
						db.updateDataType(ProxyService.TYPE_SPEED);
						db.updateSendInterval(UPDATES_GOOD_STATUS);
						
						// Start listening for location updates
						startListeningForLocation(p);
						
						// Get best position
						if (p.equals(LocationManager.NETWORK_PROVIDER)) {
							l = waitForBetterPrecision(NET_POSITION_ACCURACY_THRESHOLD);
						}
						else {
							l = waitForBetterPrecision(GPS_POSITION_ACCURACY_THRESHOLD);
						}
						newTimestamp = System.currentTimeMillis();
						
						// Stop listening for location updates
						stopListeningForLocation();
						
						// Compute speed
						if (l != null) {
							Log.d("DataService", "Found a location");
							computeSpeed(l, newTimestamp);
							
							db.updateTimestamp(newTimestamp);
							db.updateLatitude(l.getLatitude());
							db.updateLongitude(l.getLongitude());
							
							// Check if thresholds violated
							if ((p.equals(LocationManager.NETWORK_PROVIDER) && l.getAccuracy() > NET_POSITION_ACCURACY_THRESHOLD) ||
							    (p.equals(LocationManager.GPS_PROVIDER) && l.getAccuracy() > GPS_POSITION_ACCURACY_THRESHOLD)) {
								db.incrementCoarseLocations();
								if (e.last_coarse_timestamp < 0) {
									db.updateLastCoarseTimestamp(newTimestamp);
								}
							}
							else {
								// If thresholds not violated, then check if this is the first time of non-violation;
								// in this case, last_coarse_timestamp is non-negative. Check, then, if this period is longer
								// than the longest period.
								if (e.last_coarse_timestamp > 0 && newTimestamp - e.last_coarse_timestamp > e.longest_coarse_period) {
									db.updateLongestCoarsePeriod(newTimestamp - e.last_coarse_timestamp);
									db.updateLastCoarseTimestamp(-1);
								}
							}
							
							e = db.getCurrent();
							
							// Pass message to ProxyService
							Log.d("DataService", "Sending message to proxy");
							Intent proxy = new Intent(DataService.this, ProxyService.class);
							proxy.putExtra(ProxyService.KEY_MODE, ProxyService.MODE_SINGLE);
							proxy.putExtra(ProxyService.KEY_TYPE, ProxyService.TYPE_SPEED);
							proxy.putExtra(ProxyService.KEY_PARAMETER, e);
							proxy.putExtra(RECEIVER_TAG, mReceiver);
							startService(proxy);
							
						}
						else {
							Log.d("DataService", "Location not found");
							// A null location means an unreachable position. It will be considered as a coarse location
							db.incrementCoarseLocations();
							if (e.last_coarse_timestamp < 0) {
								db.updateLastCoarseTimestamp(newTimestamp);
							}
						}
					}
					else {
						// No location provider were available!
						showToast(R.string.toast_no_provider);
					}
					
				}
				else { // 3G or no connectivity
					Log.i("DataService", "Network: 3G or absent");
					
					// Location by GPS
					
					Location l;
					
					String p = getBestProvider(LocationManager.GPS_PROVIDER);
					if (p != null) {

						db.incrementNLocations();
						// Statistics shown on UI
						if (isIn3G())
							db.updateNetType(DBParameters.NET_TYPE_3G);
						else
							db.updateNetType(DBParameters.NET_TYPE_NONE);
						db.updateSendType(ProxyService.MODE_AGGREGATE);
						db.updateDataType(ProxyService.TYPE_SPEED);
						db.updateSendInterval(UPDATES_GOOD_STATUS);
						
						// Start listening for location updates
						startListeningForLocation(p);

						// Get best position
						if (p.equals(LocationManager.NETWORK_PROVIDER)) {
							l = waitForBetterPrecision(NET_POSITION_ACCURACY_THRESHOLD);
						}
						else {
							l = waitForBetterPrecision(GPS_POSITION_ACCURACY_THRESHOLD);
						}
						newTimestamp = System.currentTimeMillis();
						
						// Stop listening for location updates
						stopListeningForLocation();
	
						// Compute speed
						if (l != null) {
							Log.d("DataService", "Found a location");
							computeSpeed(l, newTimestamp);
						
							db.updateTimestamp(newTimestamp);
							db.updateLatitude(l.getLatitude());
							db.updateLongitude(l.getLongitude());
							
							// Check if thresholds violated
							if ((p.equals(LocationManager.NETWORK_PROVIDER) && l.getAccuracy() > NET_POSITION_ACCURACY_THRESHOLD) ||
							    (p.equals(LocationManager.GPS_PROVIDER) && l.getAccuracy() > GPS_POSITION_ACCURACY_THRESHOLD)) {
								db.incrementCoarseLocations();
								if (e.last_coarse_timestamp < 0) {
									db.updateLastCoarseTimestamp(newTimestamp);
								}
							}
							else {
								// If thresholds not violated, then check if this is the first time of non-violation;
								// in this case, last_coarse_timestamp is non-negative. Check, then, if this period is longer
								// than the longest period.
								if (e.last_coarse_timestamp > 0 && newTimestamp - e.last_coarse_timestamp > e.longest_coarse_period) {
									db.updateLongestCoarsePeriod(newTimestamp - e.last_coarse_timestamp);
									db.updateLastCoarseTimestamp(-1);
								}
							}

							e = db.getCurrent();

							// Pass message to ProxyService
							Log.d("DataService", "Sending message to proxy");
							Intent proxy = new Intent(DataService.this, ProxyService.class);
							proxy.putExtra(ProxyService.KEY_MODE, ProxyService.MODE_AGGREGATE);
							proxy.putExtra(ProxyService.KEY_TYPE, ProxyService.TYPE_SPEED);
							proxy.putExtra(ProxyService.KEY_PARAMETER, e);
							proxy.putExtra(RECEIVER_TAG, mReceiver);
							startService(proxy);
							
						}
						else {
							Log.d("DataService", "Location not found");
							// A null location means an unreachable position. It will be considered as a coarse location
							db.incrementCoarseLocations();
							if (e.last_coarse_timestamp < 0) {
								db.updateLastCoarseTimestamp(newTimestamp);
							}
						}
					}
					else {
						// No location provider were available!
						showToast(R.string.toast_no_provider);
					}
					
				}

				try {
					lock.lock();
					
					if (isRunning()) {
						Log.d("DataService", "Thread not interrupted, next alarm in " + UPDATES_GOOD_STATUS + " ms");
						AlarmManager alarm = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
						Intent i = new Intent(getApplicationContext(), DataService.class);
						PendingIntent p = PendingIntent.getService(getApplicationContext(), 0, i, PendingIntent.FLAG_UPDATE_CURRENT);
						alarm.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime()  + UPDATES_GOOD_STATUS, UPDATES_GOOD_STATUS, p);
					}
					else {
						Log.d("DataService", "Thread interrupted, ending alarms");
					}
					
				} finally {
					lock.unlock();
				}
			}
			else { // Critical battery
				Log.i("DataService", "Critical battery status");
				if (isWifi) {
					Log.i("DataService", "Network: wifi");
					// Do nothing
				}
				else { // 3G or no connectivity
					Log.i("DataService", "Network: 3G or absent");
					
					// Location by Network
					
					Location l;
					
					String p = getBestProvider(LocationManager.NETWORK_PROVIDER);
					if (p != null) {
						
						db.incrementNLocations();
						// Statistics shown on UI
						if (isIn3G())
							db.updateNetType(DBParameters.NET_TYPE_3G);
						else
							db.updateNetType(DBParameters.NET_TYPE_NONE);
						db.updateSendType(ProxyService.MODE_SINGLE);
						db.updateDataType(ProxyService.TYPE_POSITION);
						db.updateSendInterval(UPDATES_CRITICAL_STATUS);
						
						// Start listening for location updates
						startListeningForLocation(p);

						// Get best position
						if (p.equals(LocationManager.NETWORK_PROVIDER)) {
							l = waitForBetterPrecision(NET_POSITION_ACCURACY_THRESHOLD);
						}
						else {
							l = waitForBetterPrecision(GPS_POSITION_ACCURACY_THRESHOLD);
						}
						newTimestamp = System.currentTimeMillis();
						
						// Stop listening for location updates
						stopListeningForLocation();

						if (l != null) {
							Log.d("DataService", "Found a location");
							db.updateTimestamp(newTimestamp);
							db.updateLatitude(l.getLatitude());
							db.updateLongitude(l.getLongitude());
							
							// Check if thresholds violated
							if ((p.equals(LocationManager.NETWORK_PROVIDER) && l.getAccuracy() > NET_POSITION_ACCURACY_THRESHOLD) ||
							    (p.equals(LocationManager.GPS_PROVIDER) && l.getAccuracy() > GPS_POSITION_ACCURACY_THRESHOLD)) {
								db.incrementCoarseLocations();
								if (e.last_coarse_timestamp < 0) {
									db.updateLastCoarseTimestamp(newTimestamp);
								}
							}
							else {
								// If thresholds not violated, then check if this is the first time of non-violation;
								// in this case, last_coarse_timestamp is non-negative. Check, then, if this period is longer
								// than the longest period.
								if (e.last_coarse_timestamp > 0 && newTimestamp - e.last_coarse_timestamp > e.longest_coarse_period) {
									db.updateLongestCoarsePeriod(newTimestamp - e.last_coarse_timestamp);
									db.updateLastCoarseTimestamp(-1);
								}
							}

							e = db.getCurrent();
							
							// Pass message to ProxyService
							Log.d("DataService", "Sending message to proxy");
							Intent proxy = new Intent(DataService.this, ProxyService.class);
							proxy.putExtra(ProxyService.KEY_MODE, ProxyService.MODE_SINGLE);
							proxy.putExtra(ProxyService.KEY_TYPE, ProxyService.TYPE_POSITION);
							proxy.putExtra(ProxyService.KEY_PARAMETER, e);
							proxy.putExtra(RECEIVER_TAG, mReceiver);
							startService(proxy);
							
						}
						else {
							Log.d("DataService", "Location not found");
							// A null location means an unreachable position. It will be considered as a coarse location
							db.incrementCoarseLocations();
							if (e.last_coarse_timestamp < 0) {
								db.updateLastCoarseTimestamp(newTimestamp);
							}
						}
					}
					else {
						// No location provider were available!
						showToast(R.string.toast_no_provider);
					}
				}
				
				try {
					lock.lock();
					
					if (isRunning()) {
						Log.d("DataService", "Thread not interrupted, next alarm in " + UPDATES_CRITICAL_STATUS + " ms");
						AlarmManager alarm = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
						Intent i = new Intent(getApplicationContext(), DataService.class);
						PendingIntent p = PendingIntent.getService(getApplicationContext(), 0, i, PendingIntent.FLAG_UPDATE_CURRENT);
						alarm.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime()  + UPDATES_CRITICAL_STATUS, UPDATES_CRITICAL_STATUS, p);
					}
					else {
						Log.d("DataService", "Thread interrupted, ending alarms");
					}
					
				} finally {
					lock.unlock();
				}
			}
			
			db.incrementNUpdates();
			
			/* ---------------------------------- */
			
			
			
			Log.i("DataService", "Message processed");
			stopSelf(msg.arg1);
		}
	}
	
	/**
	 * Check if the required provider is available and returns itself if so, otherwise look for alternatives.
	 * 
	 * @param preferredProvider can be LocationManager.GPS_PROVIDER or LocationManager.NETWORK_PROVIDER
	 * @return the preferredProvider if available and active or the other one if non available or active or null otherwise
	 */
	private String getBestProvider(String preferredProvider) {
		String p = null;
		if (hasProvider(preferredProvider)) {
			p = preferredProvider;
		}
		else {
			if (preferredProvider.equals(LocationManager.GPS_PROVIDER) && hasProvider(LocationManager.NETWORK_PROVIDER)) {
				p = LocationManager.NETWORK_PROVIDER;
			}
			if (preferredProvider.equals(LocationManager.NETWORK_PROVIDER) && hasProvider(LocationManager.GPS_PROVIDER)) {
				p = LocationManager.GPS_PROVIDER;
			}
		}
		Log.d("DataService", "getBestProvider(): required " + preferredProvider + " provider, found " + p + " provider");
		return p;
	}
	
	private boolean hasProvider(String provider) {
		return (locationManager.getAllProviders().contains(provider) && locationManager.isProviderEnabled(provider));
	}
	
	private boolean isInWifi() {
		ConnectivityManager conMan = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		State wifi = conMan.getNetworkInfo(1).getState();
		return (wifi == NetworkInfo.State.CONNECTED || wifi == NetworkInfo.State.CONNECTING);
	}
	
	private boolean isIn3G() {
		ConnectivityManager conMan = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		State mobile = conMan.getNetworkInfo(0).getState();
		return (mobile == NetworkInfo.State.CONNECTED || mobile == NetworkInfo.State.CONNECTING);
	}
	
	private void startListeningForLocation(String provider) {
		Log.d("DataService", "startListeningForLocation()");
		locationHandler = new LocationHandler("Location Looper");
		locationHandler.startListening(provider);
		
	}
	
	private void stopListeningForLocation() {
		Log.d("DataService", "stopListeningForLocation()");
		locationHandler.stopListening();
		locationHandler = null;
	}
	
	private void computeSpeed(Location l, double newTimestamp) {
		double newLatitude = l.getLatitude();
		double newLongitude = l.getLongitude();
		
		if (e.latitude == 320 || e.longitude == 320) {
			db.updateCurrentSpeed(0.);
			db.updateMeanSpeed(0.);
		}
		else {
			
			if (e.mean_speed == -2) {
				// Previously it was requested to server to compute mean speed, but there was an error and mean speed were not updated anymore.
				// Now the mean value is required, so reset to zero the parameter an restart.
				db.updateCurrentSpeed(0.);
				db.updateMeanSpeed(0.);
			}
			
			float[] results = new float[1];
			double interval = (newTimestamp - e.timestamp) / 1000.;
			Location.distanceBetween(e.latitude, e.longitude, newLatitude, newLongitude, results);
			Log.i("DataService", "Recorded distance: " + results[0] + "m in " + interval + "s");
			double speed = results[0] / interval; // m/s
			speed *= 3.6; // km/h
			if (speed < 0.5 /* km/h */) {
				speed = 0; // Remove insignificant values
			}
			db.updateCurrentSpeed(speed);
			
			double mean = ALPHA * speed + (1 - ALPHA) * e.mean_speed;
			db.updateMeanSpeed(mean);
		}
	}
	
	private Location waitForBetterPrecision(int meters) {
		long timeout = POSITION_TIMEOUT / 100; // Checks every 100 ms
		Location l = null;
		
		/*if (provider.equals(LocationManager.GPS_PROVIDER)) {
			// Delete last known position 
			// http://stackoverflow.com/questions/14548707/android-how-to-reset-and-download-a-gps-data
			Bundle extras = new Bundle();
			extras.putBoolean("position", true);
			locationManager.sendExtraCommand(provider, "delete_aiding_data", extras);
			//l = locationManager.getLastKnownLocation(provider);
			try {
				// If GPS Provider wait for a quick fix
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				Log.w("DataService", "waitForBetterPrecision(): interrupted while sleeping");
				return locationManager.getLastKnownLocation(provider);
			}
		}
		
		try {
			// Wait other 2 seconds if using GPS (the more, the better)
			// or wait at least 2 seconds if using Network.
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			Log.w("DataService", "waitForBetterPrecision(): interrupted while sleeping");
			return locationManager.getLastKnownLocation(provider);
		}*/
		
		do {
			try {
				locationLock.lock();
				if (listenerLocation != null) {
					if (listenerLocation.hasAccuracy() && listenerLocation.getAccuracy() <= meters)
						return listenerLocation;
					l = listenerLocation;
				}
			} finally {
				locationLock.unlock();
			}
			try {
				Thread.sleep(100);
				/* WARNING!!!
				 * This may cause the thread to stall. If you have listeners waiting for messages this sleep will
				 * delay the delivery because the looper is stuck.
				 */
			} catch (InterruptedException e) {
				Log.w("DataService", "waitForBetterPrecision(): interrupted while sleeping");
				return l;
			}
		} while (--timeout > 0);
		return l;
	}
	
	/**
	 * Show a toast message even if not called from the UI Looper.
	 * Obtain the main UI Looper of the application and launch a message on that.
	 * 
	 * @param resID resource ID of a string to show
	 */
	private void showToast(final int resID) {
		// Show a toast message on a UI thread
		Handler h = new Handler(getApplicationContext().getMainLooper());
		h.post(new Runnable() {
			@Override
			public void run() {
				Toast.makeText(getApplicationContext(), resID, Toast.LENGTH_SHORT).show();
			}
		});
	}
	
	public void startAlarm() {
		Log.d("DataService", "startAlarm()");
		
		db.clearAll();
		
		try {
			DAOSendQueue dao = new DAOSendQueue(getApplicationContext());
			dao.open();
			dao.clearTable();
			dao.close();
		} catch (SQLiteException e) {
			Log.e("DataService", "Cannot open SendQueue DB in write mode");
		}
		
		try {
			lock.lock();

			AlarmManager alarm = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
			Intent i = new Intent(getApplicationContext(), DataService.class);
			PendingIntent p = PendingIntent.getService(getApplicationContext(), 0, i, PendingIntent.FLAG_UPDATE_CURRENT);
			alarm.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime()  + 1000, UPDATES_GOOD_STATUS, p);
			
		} finally {
			lock.unlock();
		}
	}
	
	public void stopAlarm() {
		Log.d("DataService", "stopAlarm()");
		
		try {
			lock.lock();

			AlarmManager alarm = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
			Intent i = new Intent(getApplicationContext(), DataService.class);
			PendingIntent p = PendingIntent.getService(getApplicationContext(), 0, i, PendingIntent.FLAG_NO_CREATE);
			
			if (p != null) {
				Log.d("DataService", "Alarm was active: stopped");
				alarm.cancel(p);
				p.cancel();
				
			}
			
		} finally {
			lock.unlock();
		}
	}
	
	
	
	/* ===== Other service functions ===== */
	
	@Override
	public void onCreate() {
		// Called when new Intent is created
		super.onCreate();
		
		Log.d("DataService", "onCreate()");
		
		// Start a separate thread where the looper and the handler will run
		HandlerThread t = new HandlerThread("DataService thread", android.os.Process.THREAD_PRIORITY_BACKGROUND); //Get an handler thread
		t.start(); // Start the thread
		mServiceLooper = t.getLooper(); // Get the looper associated to the handler thread
		mServiceHandler = new ServiceHandler(mServiceLooper); // Create a ServiceHandler object passing the looper
		// This thread will end when stopSelf() method will be called from the handleMessage() method
		
		// Get a DB instance
		db = new DBParameters(getApplicationContext());
		try {
			db.open();
		} catch (SQLiteException e) {
			Log.e("DataService", "Cannot open DB in write mode");
		}
		//db.fetchAll();

		locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE); // Just getting an istance of the manager
		
		mReceiver = new IPC(new Handler());
		mReceiver.setReceiver(new Receiver() {
			
			@Override
			public void onReceiveResult(int resultCode, Bundle resultData) {
				Log.d("DataService", "Received message from ProxyService");
				/*Parameter speed = (Parameter) resultData.getSerializable(PARAMETER_TAG);
				db.updateCurrentSpeed(speed.current_speed);
				db.updateMeanSpeed(speed.mean_speed);*/
				
				/* ProxyService generate a message when a speed was requested to the server and the response was
				 * saved in the DB.
				 * Force an update of object e.
				 */
				e = db.fetchAll();
			}
		});
	}
	
	@Override
	public void onDestroy() {
		// Called when all binds disconnected and all messages was processed
		super.onDestroy();
		
		Log.d("DataService", "onDestroy()");
		
		db.close();
		
		mReceiver.setReceiver(null);
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		// Called when startService() is invoked
		Log.d("DataService", "onStartCommand()");
		
		Message msg = mServiceHandler.obtainMessage();
		msg.arg1 = startId;
		mServiceHandler.sendMessage(msg);
		
		return START_STICKY;
	}
	
	
	
	/* ===== Bind operations ===== */
	
	private final IBinder mBinder = new LocalBinder();
	
	public class LocalBinder extends Binder {
		DataService getService() {
			Log.d("DataService", "getService()");
			return DataService.this;
		}
	}
	
	@Override
	public IBinder onBind(Intent intent) {
		Log.d("DataService", "onBind()");
		return mBinder;
	}

}
