package it.uniroma2.msaandroid;

import it.uniroma2.msaandroid.sqlite.DBParameters;
import it.uniroma2.msaandroid.sqlite.Parameter;
import android.app.Fragment;
import android.content.Context;
import android.database.sqlite.SQLiteException;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class FragmentStatistics extends Fragment {
	public final static long SCREEN_REFRESH = 5000;
	private Context context;
	
	private TextView latitude;
	private TextView longitude;
	private TextView currentSpeed;
	private TextView battlev;
	private TextView dataUsage;
	private TextView longestCoarse;

	private TextView meanSpeed;
	private TextView meanDrain;
	
	private TextView nupdates;
	private TextView battstat;
	private TextView netType;
	private TextView coarseLocations;
	private TextView sendingType;
	private TextView updatingInterval;
	
	private TextView textErrors;
	
	private DBParameters db;
	private Parameter e;
	
	private AsyncTask<Void, Void, Void> updates;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_statistics, container,
				false);
		
		Log.d("FragmentStatistics", "onCreateView()");
		
		context = getActivity().getApplicationContext(); // getActivity() can be null if this fragment is detached. Better save the context for further uses.
		
		latitude = (TextView) rootView.findViewById(R.id.lat_value);
		longitude = (TextView) rootView.findViewById(R.id.lon_value);
		currentSpeed = (TextView) rootView.findViewById(R.id.speed_value);
		battlev = (TextView) rootView.findViewById(R.id.batt_perc_value);
		dataUsage = (TextView) rootView.findViewById(R.id.data_usage_value);
		longestCoarse = (TextView) rootView.findViewById(R.id.longest_coarse_value);
		
		meanSpeed = (TextView) rootView.findViewById(R.id.average_speed_value);
		meanDrain = (TextView) rootView.findViewById(R.id.average_drain_value);
		
		nupdates = (TextView) rootView.findViewById(R.id.num_update_value);
		battstat = (TextView) rootView.findViewById(R.id.batt_status_value);
		netType = (TextView) rootView.findViewById(R.id.net_type_value);
		coarseLocations = (TextView) rootView.findViewById(R.id.coarse_value);
		sendingType = (TextView) rootView.findViewById(R.id.sending_type_value);
		updatingInterval = (TextView) rootView.findViewById(R.id.updating_interval_value);

		textErrors = (TextView) rootView.findViewById(R.id.error_value);
		
		db = new DBParameters(context);
		try {
			db.openRO();
		} catch (SQLiteException e) {
			Log.e("DataService", "Cannot open DB in Read Only mode");
		}
		
		return rootView;
	}
	
	@Override
	public void onStart() {
		super.onStart();
		
		Log.d("FragmentStatistics", "onStart()");
		
		e = db.fetchAll();
		updateFields(e);
		
		updates = new AsyncTask<Void, Void, Void>() {

			@Override
			protected Void doInBackground(Void... params) {
				while(true) {
					try {
						if (Thread.currentThread().isInterrupted())
							break;
						Thread.sleep(SCREEN_REFRESH);
						publishProgress();
					} catch (InterruptedException e) {
						Log.d("FragmentStatistics", "Updates interrupted");
						break;
					}
				}
				return null;
			}
			
			protected void onProgressUpdate(Void... voids) {
				Log.d("FragmentStatistics", "AsyncTask: updating...");
				e = db.fetchAll();
				updateFields(e);
			};
			
		}.execute();
	}
	
	private void updateFields(Parameter e) {
		if (e.latitude != 320) {
			latitude.setText(String.format("%.6f °", e.latitude));
		}
		if (e.longitude != 320) {
			longitude.setText(String.format("%.6f °", e.longitude));
		}
		if (e.current_speed != -1) {
			if (e.current_speed == -2) {
				currentSpeed.setText(R.string.serv_error_text);
			}
			else {
				currentSpeed.setText(String.format("%.3f Km/h", e.current_speed));
			}
		}
		if (e.battery_level != -1) {
			battlev.setText(String.format("%.2f %%", (e.battery_level * 100)));
		}
		dataUsage.setText(String.format("%d bytes", e.data_usage));
		longestCoarse.setText(String.format("%.2f s", (double)e.longest_coarse_period / 1000));

		if (e.mean_speed != -1) {
			if (e.mean_speed == -2) {
				meanSpeed.setText(R.string.serv_error_text);
			}
			else {
				meanSpeed.setText(String.format("%.3f Km/h", e.mean_speed));
			}
		}
		if (e.battery_status != null) {
			if (e.battery_status.equals(DBParameters.STATUS_CHARGING)) {
				battstat.setText(R.string.batt_plugged);
				meanDrain.setText(R.string.null_text);
			}
			else if (e.battery_status.equals(DBParameters.STATUS_DISCHARGING)) {
				battstat.setText(R.string.batt_unplugged);
				if (e.battery_drain_rate != -1) {
					meanDrain.setText(String.format("%.3f %%/h", e.battery_drain_rate));
				}
			}
		}
		
		nupdates.setText(String.valueOf(e.nupdates));
		if (e.net_type == DBParameters.NET_TYPE_WIFI) {
			netType.setText(R.string.net_wifi);
		}
		else {
			if (e.net_type == DBParameters.NET_TYPE_3G) {
				netType.setText(R.string.net_3g);
			}
			else {
				if (e.net_type == DBParameters.NET_TYPE_NONE) {
					netType.setText(R.string.net_absent);
				}
				else {
					netType.setText(R.string.null_text);
				}
			}
		}
		if (e.nlocations > 0) {
			coarseLocations.setText(String.format("%.2f %%", e.coarse_locations / (double)e.nlocations * 100));
		}
		String send = "";
		if (e.send_type != null) {
			if (e.send_type.equals(ProxyService.MODE_AGGREGATE)) {
				send = getResources().getString(R.string.mode_aggregate) + " ";
			}
			else {
				send = getResources().getString(R.string.mode_single) + " ";
			}
		}
		if (e.data_type != null) {
			if (e.data_type.equals(ProxyService.TYPE_SPEED)) {
				send += getResources().getString(R.string.type_speed);
			}
			else {
				send += getResources().getString(R.string.type_location);
			}
		}
		if (send.length() > 0) {
			sendingType.setText(send);
		}
		if (e.send_interval > 0) {
			updatingInterval.setText(String.format("%d ms", e.send_interval));
		}
		
		// Check errors
		if (e.alert_message != null) {
			textErrors.setText(e.alert_message);
			textErrors.setTextColor(Color.RED);
			textErrors.setTypeface(null, Typeface.BOLD_ITALIC);
		}
		else {
			textErrors.setText(R.string.null_text);
			textErrors.setTextColor(Color.BLACK); // Should be the default color for the theme
			textErrors.setTypeface(null, Typeface.ITALIC);
		}
	}
	
	@Override
	public void onStop() {
		super.onStop();
		updates.cancel(true);
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		db.close();
	}
}
