package it.uniroma2.msaandroid.network;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import android.util.Log;

public class NetConnection {
	//private final static String USERNAME = "";
	//private final static String PASSWORD = "";
	
	HttpURLConnection connection;
	
	public NetConnection(String method, String url) throws IOException {
		URL urlObj;
		
		urlObj = new URL(url);
		
		connection = (HttpURLConnection) urlObj.openConnection();
		connection.setRequestMethod(method);
		
		if (method.equalsIgnoreCase("POST") || method.equalsIgnoreCase("PUT")) {
			connection.setDoOutput(true);
			connection.setRequestProperty("Content-Type", "application/json");
		}
		
		connection.setDoInput(true);
		connection.setRequestProperty("Accept", "application/json");
		//String credentials = USERNAME + ":" + PASSWORD;
		//connection.setRequestProperty("Authorization", "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.DEFAULT));
	}
	
	public String readString() throws IOException {
		InputStream content = connection.getInputStream();
		BufferedReader in = new BufferedReader (new InputStreamReader(content));
		String line;
		String string = "";
		while ((line = in.readLine()) != null) {
			string += line+"\n";
		}
		content.close();
		in.close();
		
		Log.d("NetConnection", "readString(): server body response: " + string);
		
		return string;
	}
	
	public void writeString(String message) throws IOException {
		Log.d("NetConnection", "writeString(): client body request: " + message);
		
		OutputStream os = connection.getOutputStream();
		DataOutputStream dos = new DataOutputStream(os);
		dos.write(message.getBytes("UTF-8"));
		dos.write('\n');
		dos.flush();
		dos.close();
		os.close();
	}
	
	public int getResponseCode() throws IOException {
		return connection.getResponseCode();
	}
	
	public String getResponseMessage() throws IOException {
		return connection.getResponseMessage();
	}
	
}
