package it.uniroma2.msaandroid.network;

public class ServerResponseException extends Exception {
	private static final long serialVersionUID = -9035799191192835819L;
	
	public ServerResponseException(String responseMessage) {
		super(responseMessage);
	}
}
