package it.uniroma2.msaandroid.network;

import java.io.Serializable;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;
import android.util.Log;

public class SLAThresholds implements Serializable {
	private static final long serialVersionUID = 1572213162595140095L;
	
	public static final String KEY_DRAIN_RATE = "drain_rate";
	public static final String KEY_DATA_USAGE = "data_usage";
	public static final String KEY_PERC_COARSE = "perc_coarse_locations";
	public static final String KEY_LONGEST_COARSE = "longest_coarse_locations";
	
	public static final float DEF_BANDWIDTH = 100f;        /* bytes/s */
	public static final float DEF_DRAIN_RATE = 0.1f;      /* % */
	public static final float DEF_DATA_USAGE = 500f;       /* bytes in the given interval of time (eg. 30 min) */
	public static final float DEF_PERC_COARSE = 0.1f;      /* [0, 1] (coarse / all) */
	public static final float DEF_LONGEST_COARSE = 120000f;  /* ms */
	
	public float drain_rate;
	public float data_usage;
	public float perc_coarse_locations;
	public float longest_coarse_period;
	
	public SLAThresholds() {
		drain_rate = 0.f;
		data_usage = 0.f;
		perc_coarse_locations = 0.f;
		longest_coarse_period = 0.f;
	}
	
	public static SLAThresholds getCachedThresholds(Context context) {
		SLAThresholds t = new SLAThresholds();
		SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
		t.drain_rate = pref.getFloat(KEY_DRAIN_RATE, DEF_DRAIN_RATE);
		t.data_usage = pref.getFloat(KEY_DATA_USAGE, DEF_DATA_USAGE);
		t.perc_coarse_locations = pref.getFloat(KEY_PERC_COARSE, DEF_PERC_COARSE);
		t.longest_coarse_period = pref.getFloat(KEY_LONGEST_COARSE, DEF_LONGEST_COARSE);

		Log.i("SLAThresholds", "Requested cached thresholds: drain_rate=" + t.drain_rate +
				"; data_usage=" + t.data_usage +
				"; perc_coarse_locations=" + t.perc_coarse_locations +
				"; longest_coarse_period=" + t.longest_coarse_period);
		
		return t;
	}
	
	public static boolean updateThresholds(SLAThresholds thresholds, Context context) {
		SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
		Editor e = pref.edit();

		e.putFloat(KEY_DRAIN_RATE, thresholds.drain_rate);
		e.putFloat(KEY_DATA_USAGE, thresholds.data_usage);
		e.putFloat(KEY_PERC_COARSE, thresholds.perc_coarse_locations);
		e.putFloat(KEY_LONGEST_COARSE, thresholds.longest_coarse_period);
		
		Log.i("SLAThresholds", "New thresholds: drain_rate=" + thresholds.drain_rate +
				"; data_usage=" + thresholds.data_usage +
				"; perc_coarse_locations=" + thresholds.perc_coarse_locations +
				"; longest_coarse_period=" + thresholds.longest_coarse_period);
		
		return e.commit();
	}
}
