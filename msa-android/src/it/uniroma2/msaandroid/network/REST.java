package it.uniroma2.msaandroid.network;

import java.io.IOException;

public class REST {
	public final static String HOST = "http://52.17.133.33/";
	public final static String PHONES = "phones/";
	public final static String SLA = "thresholds/";
	
	/**
	 * Used to send single or multiple phone states
	 * @param imei
	 * @param message A serialized JSONObject
	 * @throws IOException
	 */
	public static String sendPhoneState(String imei, String message) throws IOException, ServerResponseException {
		NetConnection connection = new NetConnection("PUT", HOST + PHONES + imei + "/");
		connection.writeString(message);
		
		int responseCode = connection.getResponseCode();
		if (responseCode == 404) {
			throw new Error404Exception(connection.getResponseMessage());
		}
		if (responseCode < 200 || responseCode > 299) {
			throw new ServerResponseException(connection.getResponseMessage());
		}
		return connection.readString();
	}
	
	public static String sendFirstPhoneState(String imei, String message) throws IOException, ServerResponseException {
		NetConnection connection = new NetConnection("POST", HOST + PHONES);
		connection.writeString(message);
		
		int responseCode = connection.getResponseCode();
		if (responseCode == 400) {
			throw new Error400Exception(connection.getResponseMessage());
		}
		if (responseCode < 200 || responseCode > 299) {
			throw new ServerResponseException(connection.getResponseMessage());
		}
		return connection.readString();
	}
	
	public static String sendSLAState(String imei, String message) throws IOException, ServerResponseException {
		NetConnection connection = new NetConnection("PUT", HOST + SLA + imei + "/");
		connection.writeString(message);
		
		int responseCode = connection.getResponseCode();
		if (responseCode < 200 || responseCode > 299) {
			throw new ServerResponseException(connection.getResponseMessage());
		}
		return connection.readString();
	}
	
	public static String getSLAThresholds() throws IOException, ServerResponseException {
		NetConnection connection = new NetConnection("GET", HOST + SLA);
		
		int responseCode = connection.getResponseCode();
		if (responseCode < 200 || responseCode > 299) {
			throw new ServerResponseException(connection.getResponseMessage());
		}
		return connection.readString();
	}
}
