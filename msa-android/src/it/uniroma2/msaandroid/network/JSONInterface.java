package it.uniroma2.msaandroid.network;

import it.uniroma2.msaandroid.sqlite.Parameter;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public class JSONInterface {
	public static String SpeedToJSON(String imei, Parameter e) {
		JSONObject j = new JSONObject();
		try {
			j.put("imei", imei);
			j.put("punctual_speed", e.current_speed);
			j.put("average_speed", e.mean_speed);
			j.put("last_latitude", e.latitude);
			j.put("last_longitude", e.longitude);
			j.put("last_update", e.timestamp);
			j.put("samples", e.nupdates);
		} catch (JSONException e1) {
			Log.e("JSONInterface", "Error creating JSONObject: " + e1.toString());
			return "{}";
		}
		
		return j.toString();
	}
	public static String PositionToJSON(String imei, Parameter e) {
		JSONObject j = new JSONObject();
		try {
			j.put("imei", imei);
			j.put("last_latitude", e.latitude);
			j.put("last_longitude", e.longitude);
			j.put("last_update", e.timestamp);
			j.put("samples", e.nupdates);
		} catch (JSONException e1) {
			Log.e("JSONInterface", "Error creating JSONObject: " + e1.toString());
			return "{}";
		}
		
		return j.toString();
	}
	
	public static String JSONsToJSONArray(List<String> l) {
		JSONArray a = new JSONArray();
		for (String j : l) {
			try {
				a.put(new JSONObject(j));
			} catch (JSONException e) {
				Log.e("JSONInterface", "Error creating JSONObject: " + e.toString());
				return "{\"phones\": []}";
			}
		}
		JSONObject o = new JSONObject();
		try {
			o.put("phones", a);
		} catch (JSONException e) {
			Log.e("JSONInterface", "Error creating JSONObject: " + e.toString());
			return "{\"phones\": []}";
		}
		return o.toString();
	}
	
	public static Parameter JSONToParameter(String json) throws JSONException {
		JSONObject j = new JSONObject(json);
		Parameter e = new Parameter();
		
		e.latitude = j.getDouble("last_latitude");
		e.longitude = j.getDouble("last_longitude");
		e.current_speed = j.getDouble("punctual_speed");
		e.mean_speed = j.getDouble("average_speed");
		e.timestamp = j.getLong("last_update");
		
		return e;
	}
	
	/* SLA */
	public static String SLAToJSON(String imei, Parameter e) {
		JSONObject j = new JSONObject();
		try {
			j.put("imei", imei);
			j.put("drain", e.battery_drain_rate);
			j.put("traffic", e.data_usage);
			j.put("perc_imprecision", e.coarse_locations / (double)e.nlocations);
			j.put("max_imprecision_interv", e.longest_coarse_period);
		} catch (JSONException e1) {
			Log.e("JSONInterface", "Error creating JSONObject: " + e1.toString());
			return "{}";
		}
		
		return j.toString();
	}
	
	public static String JSONToSLAalert(String json) {
		try {
			JSONObject j = new JSONObject(json);
			if (j.getBoolean("alert")) {
				return j.getString("problems");
			}
		} catch (JSONException e1) {
			Log.i("JSONInterface", "No SLA violation message found. Exception message: " + e1.toString());
		}
		return null;
	}
	
	public static SLAThresholds JSONToSLAThresholds(String json) throws JSONException {
		JSONArray a = new JSONArray(json);
		SLAThresholds t = new SLAThresholds();
		
		
		for (int i = 0; i < a.length(); i++) {
			JSONObject j = a.getJSONObject(i);
			
			if (j.getString("name").equals("drain")) {
				t.drain_rate = (float) j.getDouble("value_float");
			}
			if (j.getString("name").equals("traffic")) {
				t.data_usage = (float) j.getDouble("value_float");
			}
			if (j.getString("name").equals("perc_imprecision")) {
				t.perc_coarse_locations = (float) j.getDouble("value_float");
			}
			if (j.getString("name").equals("max_imprecision_interv")) {
				t.longest_coarse_period = (float) j.getDouble("value_float");
			}
		}
		
		return t;
	}
}
