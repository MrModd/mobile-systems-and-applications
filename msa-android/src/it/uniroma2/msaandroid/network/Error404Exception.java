package it.uniroma2.msaandroid.network;

public class Error404Exception extends ServerResponseException {
	private static final long serialVersionUID = -2554036919608244197L;
	
	public Error404Exception(String responseMessage) {
		super(responseMessage);
	}
}
