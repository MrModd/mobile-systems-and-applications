package it.uniroma2.msaandroid;

import it.uniroma2.msaandroid.DataService.LocalBinder;
import it.uniroma2.msaandroid.network.JSONInterface;
import it.uniroma2.msaandroid.network.REST;
import it.uniroma2.msaandroid.network.SLAThresholds;
import it.uniroma2.msaandroid.network.ServerResponseException;

import java.io.IOException;

import org.json.JSONException;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;

public class MainActivity extends Activity {
	private ProgressBar loading;
	private TextView defText;
	private Fragment statistics = null;
	private Switch toggleServicesSwitch = null;
	
	private DataService dataService = null;
	private Intent dataServiceIntent = null;
	private boolean serviceBound = false;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) { // FIRST (not called on resuming)
		super.onCreate(savedInstanceState);
		
		Log.d("MainActivity", "onCreate()");
		
		setContentView(R.layout.activity_main);
		
		defText = (TextView) findViewById(R.id.textView_def_message);
		
	}
	
	@Override
	protected void onStart() { // SECOND
		super.onStart();
		
		Log.d("MainActivity", "onStart()");
		
		bindService();
		
		// We don't have an instance of the toggle button yet,
		// we cannot setup the UI for now.
		// Waiting for onCreateOptionsMenu to be called.
	}
	
	private void bindService() { //THIRD
		Log.d("MainActivity", "bindService(): \"serviceBound\" is: " + serviceBound);
		if (!serviceBound) {
			dataServiceIntent = new Intent(this, DataService.class);
			bindService(dataServiceIntent, mConnection, Context.BIND_AUTO_CREATE);
			serviceBound = true;
		}
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) { // FOURTH (not called on resuming)
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		
		Log.d("MainActivity", "onCreateOptionsMenu()");

		loading = (ProgressBar) menu.findItem(R.id.progress_menu_bar).getActionView();
		toggleServicesSwitch = (Switch) menu.findItem(R.id.switch_button).getActionView();
		
		toggleServicesSwitch.setEnabled(false);
		loading.setVisibility(View.VISIBLE);
		
		new AsyncTask<Void, Void, Void>() {

			@Override
			protected Void doInBackground(Void... params) {
				
				try {
					String thresholds = REST.getSLAThresholds();
					SLAThresholds t = JSONInterface.JSONToSLAThresholds(thresholds);
					SLAThresholds.updateThresholds(t, MainActivity.this);
				} catch (IOException | ServerResponseException | JSONException e) {
					Log.e("MainActivity", "Unable to fetch thresholds from server: " + e.getMessage());
				}
				
				return null;
			}
			
			protected void onPostExecute(Void result) {
				toggleServicesSwitch.setEnabled(true);
				loading.setVisibility(View.INVISIBLE);
			};
			
		}.execute();
		
		if (dataService != null && dataService.isRunning()) {
			Log.d("MainActivity", "onCreateOptionsMenu(): dataService is running");
			toggleServicesSwitch.setChecked(true);
			setServiceUI();
		}
		
		toggleServicesSwitch.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				
				Log.d("MainActivity", "setOnCheckedChangeListener(): toggle switch status changed: " + isChecked);
				if (isChecked) {
					dataService.startAlarm();
					setServiceUI();
				}
				else {
					dataService.stopAlarm();
					unsetServiceUI();
				}
				
			}
		});
		
		return true;
	}
	
	private ServiceConnection mConnection = new ServiceConnection() {

		@Override
		public void onServiceConnected(ComponentName name, IBinder service) { // FIFTH
			// When the service is finally connected change the UI adding the fragment
			
			Log.d("MainActivity", "onServiceConnected()");
			
			LocalBinder binder = (LocalBinder) service;
			dataService = binder.getService();
			
			if (toggleServicesSwitch != null) {
				// If already instantiated, this onStart() call was made from a pre-existent
				// activity (for instance, the app wasn't killed before, but just brought
				// on the background with the home button)
				
				// Must restore the UI!
				if (dataService.isRunning()) {
					Log.d("MainActivity", "onServiceConnected(): dataService is running");
					toggleServicesSwitch.setChecked(true);
					//setServiceUI(); // No need for this: previous instruction will call onCheckedChanged()
				}
			}
		}
		
		@Override
		public void onServiceDisconnected(ComponentName name) {
			//This method is never called unless service crashed or stopped working
			Log.d("MainActivity", "onServiceDisconnected()");
		}
		
	};
	
	@Override
	protected void onStop() { // SIXTH
		super.onStop();
		
		Log.d("MainActivity", "onStop()");
		
		unbindService();
	}
	
	private void unbindService() { // SEVENTH
		Log.d("MainActivity", "unbindService(): \"serviceBound\" is: " + serviceBound);
		if (serviceBound) {
			unbindService(mConnection);
			serviceBound = false;
			dataService = null;
		}
	}
	
	private void setServiceUI() {
		statistics = new FragmentStatistics();
		
		// Change UI
		defText.setVisibility(View.INVISIBLE);
		FragmentManager fm = getFragmentManager();
		FragmentTransaction ft = fm.beginTransaction();
		ft.replace(R.id.container, statistics);
		ft.commit();
	}
	
	private void unsetServiceUI() {
		// Change UI
		FragmentManager fm = getFragmentManager();
		FragmentTransaction ft = fm.beginTransaction();
		ft.remove(statistics);
		ft.commit();
		defText.setVisibility(View.VISIBLE);
		statistics = null;
	}
}
