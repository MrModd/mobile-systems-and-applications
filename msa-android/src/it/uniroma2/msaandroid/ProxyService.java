package it.uniroma2.msaandroid;

import it.uniroma2.msaandroid.network.Error400Exception;
import it.uniroma2.msaandroid.network.Error404Exception;
import it.uniroma2.msaandroid.network.JSONInterface;
import it.uniroma2.msaandroid.network.REST;
import it.uniroma2.msaandroid.network.SLAThresholds;
import it.uniroma2.msaandroid.network.ServerResponseException;
import it.uniroma2.msaandroid.sqlite.DAOSendQueue;
import it.uniroma2.msaandroid.sqlite.DBParameters;
import it.uniroma2.msaandroid.sqlite.Parameter;

import java.io.IOException;

import org.json.JSONException;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteException;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.ResultReceiver;
import android.preference.PreferenceManager;
import android.telephony.TelephonyManager;
import android.util.Log;

public class ProxyService extends Service {
	public static final String KEY_MODE = "mode";
	public static final String MODE_SINGLE = "single";
	public static final String MODE_AGGREGATE = "aggregate";
	
	public static final String KEY_TYPE = "type";
	public static final String TYPE_SPEED = "speed";
	public static final String TYPE_POSITION = "position";
	
	public static final String KEY_PARAMETER = "parameter";
	
	public static final int AGGREGATION_LEVEL = 10;
	
	private String imei;
	private boolean registered;
	private String serverResponse;
	
	private Looper mServiceLooper;
	private ServiceHandler mServiceHandler;
	
	private ResultReceiver mReceiver;
	
	private DBParameters db;
	private DAOSendQueue dao;
	
	private final class ServiceHandler extends Handler {
		public ServiceHandler(Looper looper) {
			super(looper);
		}
		
		@Override
		public void handleMessage(Message msg) {
			
			Log.i("ProxyService", "Handling message");
			
			Bundle b = msg.getData();
			String mode = b.getString(KEY_MODE);
			String type = b.getString(KEY_TYPE);
			Parameter e = (Parameter) b.getSerializable(KEY_PARAMETER);
			mReceiver = b.getParcelable(DataService.RECEIVER_TAG);
			
			if (mode != null && type != null && e != null) {
				
				if (mode.equals(MODE_SINGLE)) {
					// Send this message
					// If other messages are present in queue send them as well
					if (type.equals(TYPE_SPEED)) {
						
						Log.d("ProxyService", "Sending message: mode single, type speed");
						String m = JSONInterface.SpeedToJSON(imei, e);
						dao.push(m);
						
						sendAll();
						String alert = sendSLA();
						db.updateAlertMessage(alert); // If null it will remove previous alarm
						
					}
					if (type.equals(TYPE_POSITION)) {
						
						Log.d("ProxyService", "Sending message: mode single, type position");
						String m = JSONInterface.PositionToJSON(imei, e);
						dao.push(m);
						
						if (sendAll()) {
							try {
								// Construct an object from REST response
								Parameter speed = JSONInterface.JSONToParameter(serverResponse);
								// Update DB
								db.updateCurrentSpeed(speed.current_speed);
								db.updateMeanSpeed(speed.mean_speed);
								// Send to DataService
								Bundle update = new Bundle();
								update.putSerializable(DataService.PARAMETER_TAG, speed);
								mReceiver.send(0, update);
							} catch (JSONException e1) {
								Log.e("ProxyService", "Server response is not a JSON. Error: " + e1.getMessage());
								// Mean speed is not anymore updated. Set to an invalid value so that DataService can relate to this
								db.updateCurrentSpeed(-2.);
								db.updateMeanSpeed(-2.);
							}
						}
						else {
							// Mean speed is not anymore updated. Set to an invalid value so that DataService can relate to this
							db.updateCurrentSpeed(-2.);
							db.updateMeanSpeed(-2.);
						}
						
						String alert = sendSLA();
						db.updateAlertMessage(alert); // If null it will remove previous alarm
						
					}
					
				}
				if (mode.equals(MODE_AGGREGATE)) {
					// Add this message to the queue
					// If the queue contains enough messages send them in bulk
					if (type.equals(TYPE_SPEED)) {
						
						Log.d("ProxyService", "Appending message: mode aggregate, type speed");
						String m = JSONInterface.SpeedToJSON(imei, e);
						dao.push(m);
						
						if (dao.size() >= AGGREGATION_LEVEL) {
							Log.d("ProxyService", "Aggregation level reached");
							sendAll();
							String alert = sendSLA();
							db.updateAlertMessage(alert); // If null it will remove previous alarm
						}
						
					}
					if (type.equals(TYPE_POSITION)) {
						
						Log.d("ProxyService", "Appending message: mode aggregate, type position");
						String m = JSONInterface.PositionToJSON(imei, e);
						dao.push(m);
						
						if (dao.size() >= AGGREGATION_LEVEL) {
							Log.d("ProxyService", "Aggregation level reached");
							
							if (sendAll()) {
								try {
									// Construct an object from REST response
									Parameter speed = JSONInterface.JSONToParameter(serverResponse);
									// Update DB
									db.updateCurrentSpeed(speed.current_speed);
									db.updateMeanSpeed(speed.mean_speed);
									// Send to DataService
									Bundle update = new Bundle();
									update.putSerializable(DataService.PARAMETER_TAG, speed);
									mReceiver.send(0, update);
								} catch (JSONException e1) {
									Log.e("ProxyService", "Server response is not a JSON. Error: " + e1.getMessage());
									// Mean speed is not anymore updated. Set to an invalid value so that DataService can relate to this
									db.updateCurrentSpeed(-2.);
									db.updateMeanSpeed(-2.);
								}
							}
							else {
								// Mean speed is not anymore updated. Set to an invalid value so that DataService can relate to this
								db.updateCurrentSpeed(-2.);
								db.updateMeanSpeed(-2.);
							}
							
							String alert = sendSLA();
							db.updateAlertMessage(alert); // If null it will remove previous alarm
							
						}
						
					}
				}
				
			} else {
				Log.w("DataService", "Invalid extra bundle arguments");
			}
			
			Log.i("DataService", "Message processed");
			
			// Terminate the service only if there's no other pending message (sent with consecutive call
			// to onStartCommand).
			// When a new onStartCommand is triggered, the start request ID is updated with the new one
			// and the startID brought by msg.arg1 of this current call is not anymore the last start
			// request ID.
			// stopSelf() close the services just in case its argument equals the ID of the last start
			// request.
			stopSelf(msg.arg1);
		}
	}
	
	private boolean sendAll() {
		Log.d("ProxyService", "sendAll()");
		String m;
		Parameter e = db.fetchAll();
		long timestamp;
		
		if (!registered) {
			m = dao.peek();
			
			try {
				
				// Send message and get the response
				timestamp = System.currentTimeMillis();
				serverResponse = REST.sendFirstPhoneState(imei, m);
				
				// Now this phone is registered with the server, update shared preference
				SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(ProxyService.this);
				pref.edit().putBoolean("Registered", true).commit();
				registered = true;
				
				// Remove sent message from the queue
				dao.pop();
				
				// Update SLA statistics
				if (e.data_usage_timestamp < 0 || e.data_usage_timestamp + DataService.NET_TRAFFIC_INTERVAL > timestamp) {
					// Reset data usage if timestamp over the time threshold
					db.updateDataUsageTimestamp(timestamp);
					db.updateDataUsage(0);
					e = db.getCurrent();
				}
				int bytes = m.getBytes("UTF-8").length/* + serverResponse.getBytes("UTF-8").length*/;
				db.updateDataUsage(e.data_usage + bytes);
				e = db.getCurrent();
				
			} catch (IOException e1) {
				// Connection error
				Log.e("ProxyService", "Cannot send message. Retrying later. Error: " + e1.getMessage());
				return false;
			} catch (Error400Exception e1) {
				// Phone already registered
				registered = true;
				SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(ProxyService.this);
				pref.edit().putBoolean("Registered", registered).commit();
				Log.e("ProxyService", "Phone already registered. Server response: " + e1.getMessage());
				// No return here:
				// this error is still recoverable
			} catch (ServerResponseException e1) {
				// Server error
				Log.e("ProxyService", "Cannot send message. Server response: " + e1.getMessage() + ". Retrying later");
				return false;
			}
		}

		if (dao.size() > 0) {
			m = JSONInterface.JSONsToJSONArray(dao.getAll());
			try {

				// Send messages and get the response
				timestamp = System.currentTimeMillis();
				serverResponse = REST.sendPhoneState(imei, m);

				// Remove sent messages from the queue
				dao.clearTable();
				
				// Update SLA statistics
				if (e.data_usage_timestamp < 0 || e.data_usage_timestamp + DataService.NET_TRAFFIC_INTERVAL > timestamp) {
					// Reset data usage if timestamp over the time threshold
					db.updateDataUsageTimestamp(timestamp);
					db.updateDataUsage(0);
					e = db.getCurrent();
				}
				int bytes = m.getBytes("UTF-8").length/* + serverResponse.getBytes("UTF-8").length*/;
				db.updateDataUsage(e.data_usage + bytes);
				
			} catch (IOException e1) {
				// Connection error
				Log.e("ProxyService", "Cannot send message. Retrying later. Error: " + e1.getMessage());
				return false;
			} catch (Error404Exception e1) {
				// Phone not registered
				registered = false;
				SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(ProxyService.this);
				pref.edit().putBoolean("Registered", registered).commit();
				Log.e("ProxyService", "Phone not registered. Server response: " + e1.getMessage());
				return false;
			} catch (ServerResponseException e1) {
				// Server error
				Log.e("ProxyService", "Cannot send message. Server response: " + e1.getMessage() + ". Retrying later");
				return false;
			}
		}
		
		return true;
	}
	
	private String sendSLA() {
		Log.d("ProxyService", "sendSLA()");
		Parameter e = db.fetchAll();
		String m, response = null;
		
		m = JSONInterface.SLAToJSON(imei, e);
		try {
			response = REST.sendSLAState(imei, m);
			response = JSONInterface.JSONToSLAalert(response);
			return response;
		} catch (IOException | ServerResponseException e1) {
			Log.e("ProxyService", "sendSLA(): cannot send SLA message. Computing locally. Error: " + e1.getMessage());
			
			// Remote retrieval of SLA violations failed, checking locally
			SLAThresholds t = SLAThresholds.getCachedThresholds(getApplicationContext());
			if (e.battery_drain_rate > t.drain_rate ||
				e.data_usage > t.data_usage ||
				e.coarse_locations / (double)e.nlocations > t.perc_coarse_locations ||
				e.longest_coarse_period > t.longest_coarse_period) {
				
				return getApplicationContext().getResources().getString(R.string.error_sla_violation);
				
			}
		}
		return null;
	}
	
	@Override
	public void onCreate() {
		Log.d("ProxyService", "onCreate()");
		HandlerThread t = new HandlerThread("ProxyService", android.os.Process.THREAD_PRIORITY_BACKGROUND);
		t.start();
		
		mServiceLooper = t.getLooper();
		mServiceHandler = new ServiceHandler(mServiceLooper);
		
		dao = new DAOSendQueue(getApplicationContext());
		try {
			dao.open();
		} catch (SQLiteException e) {
			Log.e("ProxyService", "Cannot open DAO in write mode");
		}
		
		db = new DBParameters(getApplicationContext());
		try {
			db.open();
		} catch (SQLiteException e) {
			Log.e("ProxyService", "Cannot open DB in write mode");
		}
		//db.fetchAll();
		
		TelephonyManager telephonyManager = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
		imei = telephonyManager.getDeviceId();
		
		SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);
		registered = pref.getBoolean("Registered", false);
	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		Log.d("ProxyService", "onStartCommand()");
		Message msg = mServiceHandler.obtainMessage();
		msg.arg1 = startId;
		msg.setData(intent.getExtras());
		mServiceHandler.sendMessage(msg);
		
		return START_STICKY;
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		Log.d("ProxyService", "onDestroy()");
		
		dao.close();
		db.close();
	}
	
	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

}
