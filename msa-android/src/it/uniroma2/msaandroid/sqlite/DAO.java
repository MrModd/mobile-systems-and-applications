package it.uniroma2.msaandroid.sqlite;

import java.util.HashMap;
import java.util.Map;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.util.Log;

/**
 * Generic class for abstract access to a db
 * table with two columns: key, value
 * @author federico
 *
 */
public class DAO {
	private final String tableName;
	
	private SQLiteDatabase db;
	private DBHelper dbHelper;
	
	public DAO(Context context, String table) {
		tableName = table;
		dbHelper = new DBHelper(context, tableName);
	}
	
	public void open() throws SQLiteException {
		Log.d("DAO", "open()");
		
		db = dbHelper.getWritableDatabase();
	}
	
	public void openRO() throws SQLiteException {
		Log.d("DAO", "openRO()");
		
		db = dbHelper.getReadableDatabase();
	}
	
	public void close() {
		Log.d("DAO", "close()");
		
		dbHelper.close();
	}
	
	public void create(String key, String value) {
		Log.d("DAO", "create()");
		
		ContentValues v = new ContentValues();
		v.put(DBHelper.COL_KEY, key);
		v.put(DBHelper.COL_VALUE, value);
		db.insert(tableName, null, v);
	}

	public void update(String key, String value) {
		Log.d("DAO", "update()");
		
		ContentValues v = new ContentValues();
		v.put(DBHelper.COL_KEY, key);
		db.update(tableName, v, DBHelper.COL_VALUE + " = '" + value + "'", null);
	}
	
	public void createOrUpdate(String key, String value) {
		String query = "INSERT OR REPLACE INTO " + tableName +
				" ( " + DBHelper.COL_KEY + ", " + DBHelper.COL_VALUE +
				" ) VALUES ( '" + key + "' , '" + value + "' );";
		Log.d("DAO", "createOrUpdate: executing query: " + query);
		
		db.execSQL(query);
	}
	
	public void delete(String key) {
		Log.d("DAO", "delete()");
		
		String[] col = {key};
		db.delete(tableName, DBHelper.COL_KEY + "= ?", col);
	}
	
	public void clearTable() {
		Log.d("DAO", "Deleting all rows of table " + tableName);
		db.delete(tableName, null, null);
	}
	
	public String get(String key) {
		Log.d("DAO", "get()");
		
		String[] col = {DBHelper.COL_VALUE};
		
		Cursor c = db.query(tableName,
				col,
				DBHelper.COL_KEY + " = '" + key + "'",
				null,
				null,
				null,
				null);
		
		if (!c.moveToFirst())
			return null;
		return c.getString(0);
	}
	
	public Map<String, String> getAll() {
		Log.d("DAO", "getAll()");
		
		Map<String, String> m = new HashMap<String, String>();
		String[] col = {DBHelper.COL_KEY, DBHelper.COL_VALUE};
		
		Cursor c = db.query(tableName, col, null, null, null, null, null);
		c.moveToFirst();
		while (!c.isAfterLast()) {
			m.put(c.getString(0), c.getString(1));
			c.moveToNext();
		}
		
		return m;
	}
}
