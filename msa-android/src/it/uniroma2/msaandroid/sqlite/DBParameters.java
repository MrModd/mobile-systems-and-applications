package it.uniroma2.msaandroid.sqlite;

import java.util.Map;

import android.content.Context;
import android.database.sqlite.SQLiteException;

public class DBParameters {
	public static final String TABLE_NAME = "parameters";

	public static final String KEY_TIMESTAMP = "timestamp";
	public static final String KEY_LAT = "latitude";
	public static final String KEY_LON = "longitude";
	public static final String KEY_CURRENT_SPEED = "current_speed";
	public static final String KEY_UPDATES = "nupdates";
	public static final String KEY_BATT_LEVEL = "battery_level";
	public static final String KEY_OLD_BATT_LEVEL = "old_battery_level";
	public static final String KEY_BATT_LEVEL_TIMESTAMP = "battery_level_timestamp";
	public static final String KEY_BATT_STATUS = "battery_status";
	
	public static final String KEY_MEAN_SPEED = "mean_speed";
	public static final String KEY_BATT_DRAIN_RATE = "battery_drain_rate";
	
	public static final String KEY_NET_TYPE = "net_type";
	public static final String KEY_SEND_TYPE = "send_type";
	public static final String KEY_DATA_TYPE = "data_type";
	public static final String KEY_SEND_INTERVAL = "send_interval";
	
	/* SLA */
	public static final String KEY_DATA_USAGE = "data_usage";
	public static final String KEY_DATA_USAGE_TIMESTAMP = "data_usage_timestamp";
	public static final String KEY_COARSE_LOCATIONS = "coarse_locations";
	public static final String KEY_NLOCATIONS = "nlocations";
	public static final String KEY_LONGEST_COARSE_PERIOD = "longest_coarse_period";
	public static final String KEY_LAST_COARSE_TIMESTAMP = "last_coarse_timestamp";
	
	public static final String KEY_ALERT_MESSAGE = "alert_message";
	
	/* Battery statuses */
	public final static String STATUS_CHARGING = "charging";
	public final static String STATUS_DISCHARGING = "discharging";
	
	/* Network type */
	public final static int NET_TYPE_NONE = 0;
	public final static int NET_TYPE_WIFI = 1;
	public final static int NET_TYPE_3G = 2;
	
	private Parameter e;
	private DAO dao;
	
	public DBParameters(Context context) {
		e = new Parameter();
		dao = new DAO(context, TABLE_NAME);
	}
	
	public void open() throws SQLiteException {
		dao.open();
	}
	
	public void openRO() throws SQLiteException {
		dao.openRO();
	}
	
	public void close() {
		dao.close();
	}
	
	public Parameter getCurrent() {
		return e;
	}
	
	public Parameter fetchAll() {
		Map<String, String> m = dao.getAll();
		String value;
		e = new Parameter();

		value = m.get(KEY_TIMESTAMP);
		if (value != null) {
			e.timestamp = Long.valueOf(value);
		}
		value = m.get(KEY_LAT);
		if (value != null) {
			e.latitude = Double.valueOf(value);
		}
		value = m.get(KEY_LON);
		if (value != null) {
			e.longitude = Double.valueOf(value);
		}
		value = m.get(KEY_CURRENT_SPEED);
		if (value != null) {
			e.current_speed = Double.valueOf(value);
		}
		value = m.get(KEY_UPDATES);
		if (value != null) {
			e.nupdates = Integer.valueOf(value);
		}
		value = m.get(KEY_BATT_LEVEL);
		if (value != null) {
			e.battery_level = Double.valueOf(value);
		}
		value = m.get(KEY_OLD_BATT_LEVEL);
		if (value != null) {
			e.old_battery_level = Double.valueOf(value);
		}
		value = m.get(KEY_BATT_LEVEL_TIMESTAMP);
		if (value != null) {
			e.battery_level_timestamp = Long.valueOf(value);
		}
		value = m.get(KEY_BATT_STATUS);
		if (value != null) {
			e.battery_status = value;
		}
		
		value = m.get(KEY_MEAN_SPEED);
		if (value != null) {
			e.mean_speed = Double.valueOf(value);
		}
		value = m.get(KEY_BATT_DRAIN_RATE);
		if (value != null) {
			e.battery_drain_rate = Double.valueOf(value);
		}
		
		value = m.get(KEY_NET_TYPE);
		if (value != null) {
			e.net_type = Integer.valueOf(value);
		}
		value = m.get(KEY_SEND_TYPE);
		if (value != null) {
			e.send_type = value;
		}
		value = m.get(KEY_DATA_TYPE);
		if (value != null) {
			e.data_type = value;
		}
		value = m.get(KEY_SEND_INTERVAL);
		if (value != null) {
			e.send_interval = Long.valueOf(value);
		}
		
		/* SLA */
		value = m.get(KEY_DATA_USAGE);
		if (value != null) {
			e.data_usage = Long.valueOf(value);
		}
		value = m.get(KEY_DATA_USAGE_TIMESTAMP);
		if (value != null) {
			e.data_usage_timestamp = Long.valueOf(value);
		}
		value = m.get(KEY_COARSE_LOCATIONS);
		if (value != null) {
			e.coarse_locations = Long.valueOf(value);
		}
		value = m.get(KEY_NLOCATIONS);
		if (value != null) {
			e.nlocations = Long.valueOf(value);
		}
		value = m.get(KEY_LONGEST_COARSE_PERIOD);
		if (value != null) {
			e.longest_coarse_period = Long.valueOf(value);
		}
		value = m.get(KEY_LAST_COARSE_TIMESTAMP);
		if (value != null) {
			e.last_coarse_timestamp = Long.valueOf(value);
		}

		value = m.get(KEY_ALERT_MESSAGE);
		if (value != null) {
			e.alert_message = value;
		}
		
		return e;
	}
	
	public void clearAll() {
		dao.clearTable();
		e = new Parameter();
	}
	
	public void updateTimestamp(long t) {
		dao.createOrUpdate(KEY_TIMESTAMP, String.valueOf(t));
		e.timestamp = t;
	}
	
	public void updateLatitude(double lat) {
		dao.createOrUpdate(KEY_LAT, String.valueOf(lat));
		e.latitude = lat;
	}
	
	public void updateLongitude(double lon) {
		dao.createOrUpdate(KEY_LON, String.valueOf(lon));
		e.longitude = lon;
	}
	
	public void updateCurrentSpeed(double speed) {
		dao.createOrUpdate(KEY_CURRENT_SPEED, String.valueOf(speed));
		e.current_speed = speed;
	}
	
	public void updateNUpdates(int n) {
		dao.createOrUpdate(KEY_UPDATES, String.valueOf(n));
		e.nupdates = n;
	}
	
	public void updateBattLevel(double lev) {
		dao.createOrUpdate(KEY_BATT_LEVEL, String.valueOf(lev));
		e.battery_level = lev;
	}
	
	public void updateOldBattLevel(double lev) {
		dao.createOrUpdate(KEY_OLD_BATT_LEVEL, String.valueOf(lev));
		e.old_battery_level = lev;
	}
	
	public void updateBattLevelTimestamp(long lev) {
		dao.createOrUpdate(KEY_BATT_LEVEL_TIMESTAMP, String.valueOf(lev));
		e.battery_level_timestamp = lev;
	}
	
	public void updateBattStatus(String status) {
		dao.createOrUpdate(KEY_BATT_STATUS, status);
		e.battery_status = status;
	}
	
	public void updateMeanSpeed(double mean) {
		dao.createOrUpdate(KEY_MEAN_SPEED, String.valueOf(mean));
		e.mean_speed = mean;
	}
	
	public void updateBattDrainRate(double drain) {
		dao.createOrUpdate(KEY_BATT_DRAIN_RATE, String.valueOf(drain));
		e.battery_drain_rate = drain;
	}
	
	public void updateNetType(int type) {
		dao.createOrUpdate(KEY_NET_TYPE, String.valueOf(type));
		e.net_type = type;
	}
	
	public void updateSendType(String type) {
		dao.createOrUpdate(KEY_SEND_TYPE, type);
		e.send_type = type;
	}
	
	public void updateDataType(String type) {
		dao.createOrUpdate(KEY_DATA_TYPE, type);
		e.data_type = type;
	}
	
	public void updateSendInterval(long interval) {
		dao.createOrUpdate(KEY_SEND_INTERVAL, String.valueOf(interval));
		e.send_interval = interval;
	}

	public void updateDataUsage(long d) {
		dao.createOrUpdate(KEY_DATA_USAGE, String.valueOf(d));
		e.data_usage = d;
	}
	
	public void updateDataUsageTimestamp(long t) {
		dao.createOrUpdate(KEY_DATA_USAGE_TIMESTAMP, String.valueOf(t));
		e.data_usage_timestamp = t;
	}
	
	public void updateCoarseLocations(long n) {
		dao.createOrUpdate(KEY_COARSE_LOCATIONS, String.valueOf(n));
		e.coarse_locations = n;
	}
	
	public void updateNLocations(long n) {
		dao.createOrUpdate(KEY_NLOCATIONS, String.valueOf(n));
		e.nlocations = n;
	}
	
	public void updateLongestCoarsePeriod(long p) {
		dao.createOrUpdate(KEY_LONGEST_COARSE_PERIOD, String.valueOf(p));
		e.longest_coarse_period = p;
	}
	
	public void updateLastCoarseTimestamp(long t) {
		dao.createOrUpdate(KEY_LAST_COARSE_TIMESTAMP, String.valueOf(t));
		e.last_coarse_timestamp = t;
	}
	
	public void updateAlertMessage(String alert) {
		if (alert == null) {
			dao.delete(KEY_ALERT_MESSAGE);
			return;
		}
		dao.createOrUpdate(KEY_ALERT_MESSAGE, alert);
		e.alert_message = alert;
	}
	
	/* Counters */
	
	public int incrementNUpdates() {
		String value = null;
		try {
			value = dao.get(KEY_UPDATES);
		} catch (SQLiteException e) {
			e.printStackTrace();
		}
		if (value != null) {
			// Store previous value + 1
			int v = Integer.valueOf(value) + 1;
			updateNUpdates(v);
			return v;
		}
		// If not set before, start from 1
		updateNUpdates(1);
		return 1;
	}
	
	public int incrementCoarseLocations() {
		String value = null;
		try {
			value = dao.get(KEY_COARSE_LOCATIONS);
		} catch (SQLiteException e) {
			e.printStackTrace();
		}
		if (value != null) {
			// Store previous value + 1
			int v = Integer.valueOf(value) + 1;
			updateCoarseLocations(v);
			return v;
		}
		// If not set before, start from 1
		updateCoarseLocations(1);
		return 1;
	}
	
	public int incrementNLocations() {
		String value = null;
		try {
			value = dao.get(KEY_NLOCATIONS);
		} catch (SQLiteException e) {
			e.printStackTrace();
		}
		if (value != null) {
			// Store previous value + 1
			int v = Integer.valueOf(value) + 1;
			updateNLocations(v);
			return v;
		}
		// If not set before, start from 1
		updateNLocations(1);
		return 1;
	}
}
