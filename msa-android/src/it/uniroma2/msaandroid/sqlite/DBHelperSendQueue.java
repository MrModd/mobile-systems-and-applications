package it.uniroma2.msaandroid.sqlite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DBHelperSendQueue extends SQLiteOpenHelper {
	public static final int DATABASE_VERSION = 2;
	
	public static final String DATABASE_NAME = "sendQueueDB";
	public static final String TABLE_NAME = "send_queue";
	
	public static final String COL_MESSAGES = "messages";
	
	private static final String CREATE_STATEMENT = "CREATE TABLE " + TABLE_NAME + " (" +
	                                               COL_MESSAGES + " TEXT);";

	public DBHelperSendQueue(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		// There's an implicit primary key, rowid, that is autoincremented
	}
	
	@Override
	public void onCreate(SQLiteDatabase db) {
		Log.i("DBHelperSendQueue", "Creating database");
		db.execSQL(CREATE_STATEMENT);
	}
	
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.w("DBHelperSendQueue", "Upgrading database from version" + oldVersion + " to version " + newVersion);
		db.execSQL("DROP TABLE IF EXISTS" + TABLE_NAME + ";");
		onCreate(db);
	}
	
}
