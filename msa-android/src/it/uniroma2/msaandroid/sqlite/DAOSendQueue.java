package it.uniroma2.msaandroid.sqlite;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.util.Log;

/**
 * Generic class for abstract access to a db
 * table with two columns: key, value
 * @author federico
 *
 */
public class DAOSendQueue {
	
	private SQLiteDatabase db;
	private DBHelperSendQueue dbHelper;
	
	public DAOSendQueue(Context context) {
		dbHelper = new DBHelperSendQueue(context);
	}
	
	public void open() throws SQLiteException {
		Log.d("DAOSendQueue", "open()");
		
		db = dbHelper.getWritableDatabase();
	}
	
	public void close() {
		Log.d("DAOSendQueue", "close()");
		
		dbHelper.close();
	}
	
	/**
	 * Push the message at the end of the queue
	 * @param message
	 */
	public void push(String message) {
		Log.d("DAOSendQueue", "push()");
		
		ContentValues v = new ContentValues();
		v.put(DBHelperSendQueue.COL_MESSAGES, message);
		db.insert(DBHelperSendQueue.TABLE_NAME, null, v);
	}
	
	/**
	 * Remove first message from queue and return it
	 * @return the first message or null if queue is empty
	 */
	public String pop() {
		Log.d("DAOSendQueue", "pop()");
		
		String[] col = {DBHelperSendQueue.COL_MESSAGES};
		
		Cursor c = db.query(DBHelperSendQueue.TABLE_NAME,
				col,
				null,
				null,
				null,
				null,
				"rowid",
				"1");
		
		if (!c.moveToFirst())
			return null;
		String m = c.getString(0);
		String[] args = {m};
		db.delete(DBHelperSendQueue.TABLE_NAME, DBHelperSendQueue.COL_MESSAGES + "= ?", args);
		return m;
	}
	
	/**
	 * Look at the first message of the queue, but don't remove it
	 * @return the first message or null if queue is empty
	 */
	public String peek() {
		Log.d("DAOSendQueue", "peek()");
		
		String[] col = {DBHelperSendQueue.COL_MESSAGES};
		
		Cursor c = db.query(DBHelperSendQueue.TABLE_NAME,
				col,
				null,
				null,
				null,
				null,
				"rowid",
				"1");
		
		if (!c.moveToFirst())
			return null;
		return c.getString(0);
	}
	
	/**
	 * Empty the queue and return all messages in a list
	 * @return the list of messages
	 */
	public List<String> popAll() {
		List<String> l = getAll();
		clearTable();
		return l;
	}
	
	/**
	 * Empty the table
	 */
	public void clearTable() {
		Log.d("DAOSendQueue", "Deleting all rows of table " + DBHelperSendQueue.TABLE_NAME);
		db.delete(DBHelperSendQueue.TABLE_NAME, null, null);
	}
	
	/**
	 * Get all messages ordered from the queue, but don't remove them
	 * @return a list of messages, it won't be null
	 */
	public List<String> getAll() {
		Log.d("DAOSendQueue", "getAll()");
		
		List<String> l = new ArrayList<String>();
		String[] col = {DBHelperSendQueue.COL_MESSAGES};
		
		Cursor c = db.query(DBHelperSendQueue.TABLE_NAME, col, null, null, null, null, null);
		c.moveToFirst();
		while (!c.isAfterLast()) {
			l.add(c.getString(0));
			c.moveToNext();
		}
		
		return l;
	}
	
	/**
	 * @return the number of elements on the queue
	 */
	public long size() {
		return DatabaseUtils.queryNumEntries(db, DBHelperSendQueue.TABLE_NAME);
	}
}
