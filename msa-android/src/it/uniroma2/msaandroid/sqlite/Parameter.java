package it.uniroma2.msaandroid.sqlite;

import java.io.Serializable;

public class Parameter implements Serializable {
	private static final long serialVersionUID = -4140255630291196948L;
	
	public long timestamp; /* Time instant of data acquisition for this instance */
	public double latitude; /* Latitude at the given timestamp */
	public double longitude; /* Longitude at the given timestamp */
	public double current_speed; /* Speed between this coordinates and the previous one */
	public int nupdates; /* Total number of updates for this session (from service start) */
	public double battery_level; /* Current battery level */
	public double old_battery_level; /* Last battery level at which battery_level_timestamp was set */
	public long battery_level_timestamp; /* Time at which old_battery_level was recorded */
	public String battery_status; /* Current battery status */
	
	public double mean_speed;
	public double battery_drain_rate;
	
	/* Parameter used by UI */
	public int net_type; /* Network type: WiFi, 3G or absent */ 
	public String send_type; /* Send type: Aggregate or single */
	public String data_type; /* Data type: Speed or location */
	public long send_interval; /* Current interval between two measurements */
	
	/* SLA parameters */
	public long data_usage; /* Transfered bytes in the time interval (see DataService for interval definition) */
	public long data_usage_timestamp; /* Beginning of the time interval for data usage count */
	public long coarse_locations; /* Number of locations that did not respect the maximum precision threshold (see DataService) */
	public long nlocations; /* Total number of locations retrieved */
	public long longest_coarse_period; /* Max time interval in which location accuracy wasn't good */
	public long last_coarse_timestamp; /* Last beginning of a low accuracy period */
	
	public String alert_message; /* Contains eventually SLA violation message */
	
	public Parameter() {
		timestamp = 0;
		latitude = 320.;
		longitude = 320.;
		current_speed = -1.;
		nupdates = 0;
		battery_level = -1.;
		old_battery_level = -1.;
		battery_level_timestamp = 0;
		battery_status = null;
		
		mean_speed = -1.;
		battery_drain_rate = -1.;
		
		net_type = -1;
		send_type = null;
		data_type = null;
		send_interval = -1;
		
		data_usage = 0;
		data_usage_timestamp = -1;
		coarse_locations = 0;
		nlocations = 0;
		longest_coarse_period = 0;
		last_coarse_timestamp = -1;
		
		alert_message = null;
	}
}
