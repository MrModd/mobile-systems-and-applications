package it.uniroma2.msaandroid.sqlite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DBHelper extends SQLiteOpenHelper {
	public static final int DATABASE_VERSION = 2;
	
	public static final String DATABASE_NAME = "mainDB";
	
	public static final String COL_KEY = "key";
	public static final String COL_VALUE = "value";
	
	private String tableName;
	
	private final String CREATE_STATEMENT;

	public DBHelper(Context context, String table) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		tableName = table;
		CREATE_STATEMENT = "CREATE TABLE " + tableName + " (" +
		                    COL_KEY + " TEXT UNIQUE NOT NULL, " +
		                    COL_VALUE + " TEXT);";
	}
	
	@Override
	public void onCreate(SQLiteDatabase db) {
		Log.i("DBHelper", "Creating database");
		db.execSQL(CREATE_STATEMENT);
	}
	
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.w("DBHelper", "Upgrading database from version" + oldVersion + " to version " + newVersion);
		db.execSQL("DROP TABLE IF EXISTS" + tableName + ";");
		onCreate(db);
	}

}
